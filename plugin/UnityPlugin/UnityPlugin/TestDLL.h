
#define DATABASE_API __declspec(dllexport)

extern "C"
{
	DATABASE_API void OpenDatabaseConnection();

	DATABASE_API void ReadFromDatabase();
}