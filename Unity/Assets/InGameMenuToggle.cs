﻿/*
 * Author : Kirsty Fraser
 * Date created : 29.6.15
 * Last amended : n.a
 * 
 * Version 1.0
 * 
 * Purpose : Check if the settings button has been pushed while in game and if show, display the in game settings panel.
 * 
 * 
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGameMenuToggle : MonoBehaviour {

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private GameObject inGamePanel;

    [SerializeField]
    private GameObject playBtn;

    [SerializeField]
    private GameObject creditsBtn;

    [SerializeField]
    private GameObject settingsBtn;

      
   public void CheckIfPlayingGame()
    {
        
       if(gameManager.ReturnState() && gameObject.name == "SettingsBtn")                        //Terrible I know but it's the last 24 hours! Send help.
       {
           //Hey we're playing the game, best show the player the settings panel.
           Debug.Log("MOVE THE PANEL");
           inGamePanel.GetComponent<UITween>().MoveOnScreen();

       }

       else if (gameManager.ReturnState() && gameObject.name == "MenuBtn")                        //Terrible I know but it's the last 24 hours! Send help.
       {
           //Hey we're playing the game, best show the player the settings panel.
           Debug.Log("MOVE THE PANEL");
           inGamePanel.GetComponent<UITween>().MoveOffScreen();
          

           //Grab the play and credits buttons and tell them to naff off to override what we've got on the OnClick piece of the button script
           playBtn.GetComponent<UITween>().MoveOffScreen();
           creditsBtn.GetComponent<UITween>().MoveOffScreen();

           //Move the settings back to where it was in game
           settingsBtn.GetComponent<UITween>().MoveInGame();

       }

    }
}
