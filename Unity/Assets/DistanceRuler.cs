﻿using UnityEngine;
using System.Collections;

public class DistanceRuler : MonoBehaviour {

	[SerializeField] private GameObject dragon;
	[SerializeField] private GameObject player;
	[SerializeField] private RectTransform dragonMarker;
    [SerializeField] private Camera gameCamera;
	[SerializeField] private float multiplier;

    [SerializeField]
    private float offset;

    [SerializeField]
    private float shakeMultiplier = 0.25f;

    [SerializeField]
    private float maxDistanceBeforeShake = 10.0f;


    private float finalFarPosition = 0.0f;
    private bool isGameStarted;
    private bool isDragonPlaying;
    private bool isDragonStopped;

    private RectTransform rectTransform;
    private UITween cameraTween;


	// Use this for initialization
	void Start () {
        rectTransform = GetComponent<RectTransform>();
        isGameStarted = false;
        cameraTween = gameCamera.GetComponent<UITween>();
    }
	
	// Update is called once per frame
    void Update()
    {
        if (isGameStarted)
        {

            float distance = player.transform.position.x - dragon.transform.position.x;

            Vector3 newPos = new Vector2(-distance * multiplier, 0f);

            finalFarPosition = offset - rectTransform.rect.width;

            if (newPos.x < finalFarPosition)
            {
                newPos.x = finalFarPosition;
            }

            else if (newPos.x > 0.0f)
            {
                newPos.x = 0.0f;
            }

            dragonMarker.anchoredPosition = newPos;         

            if (distance < maxDistanceBeforeShake)
            {
                float shakeAmount = (distance - maxDistanceBeforeShake - 1f) * shakeMultiplier;
               cameraTween.PunchPositionCheck(new Vector3(0.0f, shakeAmount, 0.0f));

                if (!isDragonPlaying)
                    UpdateDragon("Play");

            }
            else if (distance > maxDistanceBeforeShake)
            {
                if (!isDragonStopped)
                {
                    UpdateDragon("Stop");
                }
            }

        }
    }
		
	

   public void GameStarted()
    {
        isGameStarted = true;
    }


    public void GamePaused()
   {
       isGameStarted = false;
   }

    public void UpdateDragon(string status)
    {
            switch(status)
            {
                case "Play":  
                    dragon.GetComponent<Dragon>().PlayParticleEffect();
                    isDragonPlaying = true;
                    isDragonStopped = false;
                    break;
                case "Stop" : 
                    dragon.GetComponent<Dragon>().StopParticleEffect();
                    isDragonStopped = true;
                    isDragonPlaying = false;
                    break;

            }    

    }
}
