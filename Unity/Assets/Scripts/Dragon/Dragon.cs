﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Control the dragon and handle collision with player
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class Dragon : MonoBehaviour {

    [SerializeField]
    private float baseSpeed = 10.0f;

    [SerializeField]
    private float smooth = 0.1f;

    [SerializeField]
    private float bonusSpeedForBeingADragon = 1.0f;
    [SerializeField]
    private float adjustDelay = 1.0f;
    [SerializeField]
    [ReadOnly]
    private float currentSpeed;
    [SerializeField]
    [ReadOnly]
    private float playerSpeed = 5.0f;

    [SerializeField]
    Player player;


    [SerializeField]
    Vector3 startPosition;

    [SerializeField]
    private float particleDelay;        //Delay between the particle system stopping and it starting again.

    private bool isInMenu;
    private float accumulatedSpeed = 0f;         //Speed that the dragon was at prior to pausing.
    private ParticleSystem fireBreathing;
    private bool areWeWithinDistance;

    //DeltaDNA tracking 
    private int startTime;
    private string startPos;
    private string endPos;
    private DeltaDNAEventBuilder eventBuilder;

    void Awake() {
        Reset();
        currentSpeed = baseSpeed;
        accumulatedSpeed = baseSpeed;
        fireBreathing = GetComponentInChildren<ParticleSystem>();
        eventBuilder = new DeltaDNAEventBuilder();

    }

    void OnEnable() {
        Messenger.AddListener("ResetAll", Reset);

		Messenger.AddListener ("Pause", Pause);
		Messenger.AddListener ("Unpause", Unpause);
    }

    void OnDisable() {
        Messenger.RemoveListener("ResetAll", Reset);

		Messenger.RemoveListener ("Pause", Pause);
		Messenger.RemoveListener ("Unpause", Unpause);
    }

    // Update is called once per frame
    void FixedUpdate() {
        StartCoroutine(GetPlayerSpeedInThePast());
        if (isInMenu) {
            currentSpeed = player.GetSpeed();
        }

        if (!isInMenu) {

            if (currentSpeed < playerSpeed) {
                currentSpeed = Mathf.Lerp(currentSpeed, playerSpeed + baseSpeed + bonusSpeedForBeingADragon, Time.deltaTime * smooth);
            }
            else if (currentSpeed > playerSpeed) {
                currentSpeed = Mathf.Lerp(currentSpeed, (playerSpeed + bonusSpeedForBeingADragon), Time.deltaTime * smooth);
            }

        }

        /*
        var position = transform.position;
        position.x += currentSpeed * Time.deltaTime;

        transform.position = position;*/


        var position = transform.position;
        position.x += currentSpeed * Time.deltaTime;
        transform.position = position;

        if (areWeWithinDistance && !fireBreathing.isPlaying) {
            fireBreathing.startDelay = particleDelay;
            fireBreathing.Play();
        }

        
    }

    IEnumerator GetPlayerSpeedInThePast() {
        var speed = player.GetSpeed();
        for (float i = 0; i < adjustDelay; i += Time.deltaTime) {
            yield return 0;
        }
        playerSpeed = speed;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Player") {
            collider.GetComponent<Player>().Kill("Dragon");
        }
    }

    public void Reset() {
        Debug.Log("Resetting Drasgon");
        currentSpeed = baseSpeed;
        transform.position = startPosition;
        isInMenu = false;
    }

    //Used when we're in menus 
    public void Pause() {
        Debug.Log("Pause dragon");
        isInMenu = true;
        accumulatedSpeed = currentSpeed;
        currentSpeed = 0;
    }

    //Used for when we've naviagated out from these menus
    public void Unpause() {
        Debug.Log("UnPause dragon");
        isInMenu = false;
        currentSpeed = accumulatedSpeed;
    }

    //Called when we're within the distance to spray fire at poor mimic 
    public void PlayParticleEffect() {
        areWeWithinDistance = true;
        StartTrackingProximity();

    }

    public void StopParticleEffect() {
        areWeWithinDistance = false;
        StopTrackingProximity();
    }

    public bool IsParticlesPlaying() {
        return fireBreathing.IsAlive();

    }

    private void StartTrackingProximity()
    {
        //TODO 
        //- set start time to be the current time
        startTime = (int)Time.time;

        //- set start position to be the dragons current position
        startPos = " " + transform.position.x + ", " + transform.position.y;

        if (transform.position.x == null)
            Debug.LogError("NO X POSITION FOUND");
    }

    private void StopTrackingProximity()
    {
        //TODO
        //- calculate time that was spent in close proximity
        int totalTime = (int)Time.time - startTime;

        endPos = " " + transform.position.x + ", " + transform.position.y;

        eventBuilder.RecordDragonProximity(startPos, endPos, totalTime);
        
    }

}