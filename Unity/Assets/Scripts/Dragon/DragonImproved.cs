﻿using UnityEngine;
using System.Collections;

public class DragonImproved : MonoBehaviour {

    [SerializeField]
    private float adjustDelay = 1.0f;

    [SerializeField]
    private float baseSpeed = 5.0f;


    [SerializeField]
    Player player;

    [SerializeField] [ReadOnly] private float currentSpeed;
    private float playerSpeed;
    // Use this for initialization
    void Start() {
        currentSpeed = 10.0f;
        playerSpeed = 5.0f;
    }

    // Update is called once per frame
    void Update() {
        StartCoroutine(GetPlayerSpeedInThePast());

        if (currentSpeed < playerSpeed) {
            currentSpeed = Mathf.Lerp(currentSpeed, playerSpeed, Time.deltaTime);
        }
        else if (currentSpeed > playerSpeed) {
            currentSpeed = Mathf.Lerp(currentSpeed, playerSpeed + baseSpeed, Time.deltaTime) ;
        }

        var position = transform.position;
        position.x += currentSpeed * Time.deltaTime;
        transform.position = position;
    }

    IEnumerator GetPlayerSpeedInThePast() {
        var speed = player.GetComponent<Rigidbody2D>().velocity.x;
        for (float i = 0; i < adjustDelay; i += Time.deltaTime) {
            yield return 0;
        }
        playerSpeed = speed;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Player") {
            Debug.Log("NOMNOMNOM");
        }
    }
}
