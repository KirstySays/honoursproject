﻿using UnityEngine;
using System.Collections;
using DeltaDNA;

public class DeltaDNAEventBuilder : MonoBehaviour {

    private DeltaDNAManager deltaDNAManager = new DeltaDNAManager();

    public void RecordNewPlayer()
    {
        deltaDNAManager.RecordAnEvent("NEWPLAYER", null);
    }

    public void RecordNewSession()
    {
        deltaDNAManager.RecordAnEvent("NEWSESSION", null);
    }
    
    public void RecordDeath(string _deathPosition, string _deathCause, int _totalCoins)
    {
        EventBuilder eventparmas = new EventBuilder()
       .AddParam("deathPositon", _deathPosition)
       .AddParam("deathType", _deathCause)
       .AddParam("totalCoins", _totalCoins);

        deltaDNAManager.RecordAnEvent("DEATH", eventparmas);
    }

    public void RecordCurrentPosition(string _currentPosition)
    {
        EventBuilder eventparmas = new EventBuilder()
        .AddParam("currentPos", _currentPosition);

        deltaDNAManager.RecordAnEvent("CURRENTPOS", eventparmas);
    }

    public void RecordPlayerInteraction(string _currentCharPos)
    {
        EventBuilder eventParams = new EventBuilder()
        .AddParam("tapPos", _currentCharPos);

        deltaDNAManager.RecordAnEvent("PLAYERINTERACTION", eventParams);
    }

    public void RecordPlayerSwingTime(string _startPos, string _endPos, int _swingTime)
    {
        EventBuilder eventParams = new EventBuilder()
            .AddParam("swingStartPos", _startPos)
            .AddParam("swingEndPos", _endPos)
            .AddParam("swingTime", _swingTime);
        deltaDNAManager.RecordAnEvent("SWINGTIME", eventParams);
    }
    

    public void RecordDragonProximity(string _startPos, string _endPos, int _time)
    {
        Debug.Log("Start Position for the dragon " + _startPos);

        EventBuilder eventParams = new EventBuilder()
            .AddParam("dragonStartPos", _startPos)
            .AddParam("dragonEndPos", _endPos)
            .AddParam("dragonProxTime", _time);
        deltaDNAManager.RecordAnEvent("PROXTIME", eventParams);
    }

    public void RecordCoinCollected(string _position)
    {
        EventBuilder eventParams = new EventBuilder()
            .AddParam("currentPos", _position);
        deltaDNAManager.RecordAnEvent("COINCOLLECTED", eventParams);
    }

    public void RecordEndOfCurrentRun(string _time)
    {
        EventBuilder eventParams = new EventBuilder()
            .AddParam("currentRunTime", _time);
        deltaDNAManager.RecordAnEvent("RUNTIME", eventParams);
    }

    public void RecordRetries(int _retries)
    {
        EventBuilder eventParams = new EventBuilder()
            .AddParam("amountOfRetries", _retries);
        deltaDNAManager.RecordAnEvent("RETRIES", eventParams);
    }

    public void RecordSectionComplete(string _sectionName)
    {
        EventBuilder eventParams = new EventBuilder()
        .AddParam("sectionName", _sectionName);
        deltaDNAManager.RecordAnEvent("SECTIONCOMPLETE", eventParams);
    }

    public void RecordCollisionWithObstacle(string _curentPos)
    {
        EventBuilder eventParams = new EventBuilder()
        .AddParam("currentPos", _curentPos);
        deltaDNAManager.RecordAnEvent("OBSTACLECOLLISION", eventParams);
    }
}
