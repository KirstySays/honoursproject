﻿using UnityEngine;
using System.Collections;
using DeltaDNA;


public class DeltaDNAManager : MonoBehaviour {

    [SerializeField]
    [ReadOnly]
    bool isConnectedToInternet = false;

    void Awake()
    {
        DDNA.Instance.Settings.BackgroundEventUpload = false;           //Disable DDNA to upload frequently to the servers..

        if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            isConnectedToInternet = true;
            Debug.Log("Are we connected to the internet " + isConnectedToInternet);
        }
        else
            Debug.LogError("We're not connected to the internet for deltaDNA to upload");

    }
	public void InstanciateDevelopmentSDK()
    {
        DDNA.Instance.StartSDK(
			"26214797784723290192662812714537",
			"http://collect7718pnnyp.deltadna.net/collect/api",
			"http://engage7718pnnyp.deltadna.net",
			DDNA.AUTO_GENERATED_USER_ID
       );
    }

	//Removed for honours
    /*public void InstanciateLiveSDK()
    {
        DDNA.Instance.StartSDK(
           "08679216361891128095575684314391",
           "http://collect5252pnnyp.deltadna.net/collect/api",
           "http://engage5252pnnyp.deltadna.net",
           DDNA.AUTO_GENERATED_USER_ID
       );
    }*/

    public void RecordAnEvent(string _eventType, EventBuilder _eventParams)
    {
        switch(_eventType)
        {
           
            case "PLAYERDEATH": 
                DDNA.Instance.RecordEvent("playerDied", _eventParams);
                PushEvents();
                break;
            case "CURRENTPOS":
                DDNA.Instance.RecordEvent("charMovement", _eventParams);
                PushEvents();
                break;
            case "PLAYERINTERACTION":
                DDNA.Instance.RecordEvent("playerTap", _eventParams);
                PushEvents();
                break;
            case "SWINGTIME":
                DDNA.Instance.RecordEvent("charSwingTime", _eventParams);
                PushEvents();
                break;
            case "PROXTIME":
                DDNA.Instance.RecordEvent("dragonProximity", _eventParams);
                PushEvents();
                break;
            case "COINCOLLECTED":
                DDNA.Instance.RecordEvent("coinCollected", _eventParams);
                PushEvents();
                break;
            case "RUNTIME":
                DDNA.Instance.RecordEvent("runTime", _eventParams);
                PushEvents();
                break;

            case "RETRIES":
                DDNA.Instance.RecordEvent("playerRetries", _eventParams);
                PushEvents();
                break;
            case "SECTIONCOMPLETE":
                DDNA.Instance.RecordEvent("sectionComplete", _eventParams);
                PushEvents();
                break;
            case "OBSTACLECOLLISION":
                DDNA.Instance.RecordEvent("obstacleCollision", _eventParams);
                PushEvents();
                break;

        }

      
       

    }

    void PushEvents()
    {
        if(isConnectedToInternet)
        {
            DDNA.Instance.Upload();
        }
       
    }
}
