﻿/*
 * Author : Kirsty Fraser
 * Date created : 22.6.15
 * Last amended : n.a
 * 
 * Version 1.0
 * 
 * Purpose : To keep track and control the flow of the game and where the camera should be when it comes to navigating menu 'screens'
 * 
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {


    private enum GameStates { MainMenu, Settings, Play, Credits, Scores, GameOver, Thanks };

    [SerializeField] private GameStates gameState;                                   //Lets us know what state the game currently is.

    [SerializeField] private int cameraOrthoSizeMax;

    [SerializeField] private int cameraOrthoSizeMin;

    [SerializeField] private Vector3 settingsPosition;

	[SerializeField] private Player player;

    [SerializeField] private Dragon dragon;

    [SerializeField] private SectionAssembler sectionAssembler;

    [SerializeField] private SectionManager sectionManager;

    [SerializeField] private DistanceRuler distanceRuler;

    [SerializeField] private bool isGamePlaying;

    private bool isNeedingReset;

    [SerializeField] private Camera menuCamera;
    [SerializeField] private Camera gameCamera;

    [SerializeField] private GameObject exitBtn;

    [SerializeField]
    private Image blackTexture;

    [SerializeField]
    private float fadeSpeed = 1.5f;

    private DeltaDNAManager deltaDNAManager;
    private DeltaDNAEventBuilder deltaDNAEventBuilder = new DeltaDNAEventBuilder();


    //DeltaDNA figures for passing up to the server
    private int amountOfRetries;
    private float startTime;
    private float totalTime;

	//PluginWindow pluginWindow;

	// Use this for initialization
	void Start () {
		isGamePlaying = false;
		isNeedingReset = false;
		amountOfRetries = 0;


		//Pause the dragon from the start
		//dragon.Pause ();
		Messenger.Broadcast ("Pause");

		blackTexture.enabled = false;
		deltaDNAManager = new DeltaDNAManager ();
		deltaDNAManager.InstanciateDevelopmentSDK ();

		//deltaDNAEventBuilder.RecordNewPlayer ();                 //Need to call this when we start the app.
		// deltaDNAManager.InstanciateLiveSDK();

		

	}
  
    void OnEnable() {
        Debug.Log("Adding event");
        Messenger<string>.AddListener("PlayerDeath", GameOver);
        Messenger<string>.AddListener("GameStateChange", ChangeState);

    }

    void OnDisable() {
        Debug.Log("Removing event");
        Messenger<string>.RemoveListener("PlayerDeath", GameOver);
        Messenger<string>.RemoveListener("GameStateChange", ChangeState);

    }


    public void OnApplicationQuit()
    {
        //Update deltaDNA on retries
        deltaDNAEventBuilder.RecordRetries(amountOfRetries);

    }
    public void GameOver(string cause) {
        Debug.Log("Game Over");
        ChangeState("GameOver");
    }
	

    void CheckState()
    {
        switch (gameState)
        {
            case GameStates.MainMenu:
                {
					//Move the camera to the main menu transform 
					menuCamera.GetComponent<CameraControl>().MoveCamera("MainMenu");
                    break;
                }
        }    
    }

    public void ChangeState(string _newState)
    {

        menuCamera.GetComponent<CameraControl>().MoveCamera(_newState);

        switch (_newState)
        {
            case "MainMenu":
                //deltaDNAEventBuilder.RecordNewPlayer();
                gameState = GameStates.MainMenu;
                amountOfRetries = 0;

                if (isGamePlaying)
                {
                    //uiManager.ChangeScreen("PLAY");
                    Messenger.Broadcast("Play");

                    //Sort the dragon out for moving again and allow input.
                   // dragon.UnPauseDragon();
                    player.GetComponent<InputModule>().EnableInput();
                    Messenger.Broadcast("UnPause");

                }
                else
                    //uiManager.ChangeScreen("MENU");
                    Messenger.Broadcast("Menu");



                break;

            case "Settings":

                if (isGamePlaying)
                {
                    // uiManager.ChangeScreen("INGAMESTTNGS");
                    Messenger.Broadcast("InGameSettings");

                    //Pause the dragon so we're not killing the player in the main menu :( 
                   // dragon.PauseDragon();
                    //player.Pause();

				Messenger.Broadcast("Pause");

                    //Stops the player from getting stuck.
                    player.GetComponent<GrapplingHook>().ReleaseGrapple();
                    player.GetComponent<InputModule>().DisableInput();

                }
                else
                    //uiManager.ChangeScreen("SETTINGS");
                    Messenger.Broadcast("Settings");

                gameState = GameStates.Settings;
                break;

            case "Play":

                if (isNeedingReset)
                {
                    Debug.Log("Resetting yo");
                    Messenger.Broadcast("ResetAll");

                    //Keep track of the amount of retries
                    amountOfRetries++;
                }

                else if (!isGamePlaying)
                {
                    //Reset as well
                    //sectionManager.Reset();
                    sectionAssembler.Reset();
                    Messenger.Broadcast("Unpause");
                   // dragon.UnPauseDragon();
                    player.StartScoring();
                    //dragon.UnPauseDragon();
                    ToggleReset();

                }

                startTime = Time.time;

                gameState = GameStates.Play;
                gameCamera.GetComponent<CameraFollow>().StartCamera();
                isGamePlaying = true;

                //uiManager.ChangeScreen("PLAY");
                Messenger.Broadcast("Play");
              
                distanceRuler.GameStarted();
                break;

            case "Credits":
                gameState = GameStates.Credits;
                //uiManager.ChangeScreen("CREDITS");
                Messenger.Broadcast("Credits");
                break;

            case "GameOver":
                gameState = GameStates.GameOver;
                //uiManager.ChangeScreen("ENDGAME");
                Messenger.Broadcast("EndGame");
                gameCamera.GetComponent<ScreenShake>().ShakeNow(0.3f, 0.1f);
                //Need to let something know to display the damn end game panel.


                gameCamera.GetComponent<CameraFollow>().StopCamera();
                gameCamera.GetComponent<UITween>().PunchPositionStop();
                distanceRuler.GetComponent<DistanceRuler>().GamePaused();
                isGamePlaying = false;
                isNeedingReset = true;
                totalTime = Time.time - startTime;
                string time = " " + totalTime;

                //TODO put this into its own function
                deltaDNAEventBuilder.RecordEndOfCurrentRun(time);

                //Messenger.Broadcast("ResetPlayer");


                break;
            case "Thanks":
                gameState = GameStates.Thanks;
                StartCoroutine("Timer");

               
                //Record an end of session 

                break;

        }


    }

    public bool ReturnState()
    {
        return isGamePlaying;
    }

    public bool ReturnNeedingReset()
    {
        return isNeedingReset;
    }

    public void ToggleReset()
    {
        isNeedingReset = false;
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public IEnumerator Timer()
    {
        yield return new WaitForSeconds(0.3f);
        StartCoroutine("FadeToClear");
        yield return new WaitForSeconds(3f);
        StartCoroutine("FadeToBlack");
    }

    IEnumerator FadeToClear()
    {
        blackTexture.enabled = true;
        while (blackTexture.color.a > 0.05f)
        {
            blackTexture.color = Color.Lerp(blackTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
            yield return 0;
        }
       
        blackTexture.color = Color.clear;
        blackTexture.enabled = false;
    }

    IEnumerator FadeToBlack()
    {
        blackTexture.enabled = true;
        while (blackTexture.color.a < 0.95f)
        {
            Debug.Log("Changing colour");
            Color newColor = new Color(0, 0, 0);
            newColor.a = Mathf.Lerp(blackTexture.color.a, 1f, fadeSpeed * Time.deltaTime);
            blackTexture.color = newColor;
            yield return 0;
        }

        blackTexture.color = Color.black;
        Application.LoadLevel(0);

    }
}
