﻿using UnityEngine;
using System.Collections;

public class FixedAngleInput : InputModule {

    [SerializeField] private float angle;
    [SerializeField] private float distance;

    override protected void Initialise()
    {
		state = InputState.Up;
    }

    override protected void CheckInput()
    {
        state = InputState.Up;
        if (Input.GetMouseButtonDown(0))
        {
            state = InputState.Pressed;
        }
        else if (Input.GetMouseButton(0))
        {
            state = InputState.Down;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            state = InputState.Released;
        }
        CalculatePoint();
    }

    private void CalculatePoint()
    {
        Vector2 direction = Quaternion.Euler(0.0f, 0.0f, -angle) * transform.up;
        point = (Vector2)transform.position + (direction * distance);       
    }
}