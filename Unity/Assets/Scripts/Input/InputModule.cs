﻿using UnityEngine;
using System.Collections;

public enum InputState
{
    Pressed = 0,
    Down = 1,
    Released = 2,
    Up = 3,
}

public abstract class InputModule : MonoBehaviour {
    public Vector2 point { get; protected set; }
    public InputState state { get; protected set; }

    private bool isEnabled = true;

	// Use this for initialization
	void Start () {
        this.Initialise();
	}
	
	// Update is called once per frame
	void Update () {
        if (isEnabled) {
            this.CheckInput();
        }
	}

    public void DisableInput() {
        isEnabled = false;
    }

    public void EnableInput() {
        isEnabled = true;
    }

    abstract protected void Initialise();
    abstract protected void CheckInput();
}
