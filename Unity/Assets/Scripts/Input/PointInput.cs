﻿using UnityEngine;
using System.Collections;

public class PointInput : InputModule {

    override protected void Initialise()
    {

    }

    override protected void CheckInput()
    {
        state = InputState.Up;
        if (Input.GetMouseButtonDown(0))
        {
            point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            state = InputState.Pressed;
        }
        else if (Input.GetMouseButton(0))
        {
            state = InputState.Down;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            state = InputState.Released;
        }
    }
}
