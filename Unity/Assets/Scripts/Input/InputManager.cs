﻿/*
 * Author : Kirsty Fraser
 * 
 * Purpose : Used for testing the parallax scrolling on the camera
 * 
 */

using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

	private float moveBy = 0.1f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKey (KeyCode.LeftArrow)) {
			float newX = transform.position.x - moveBy;
			transform.position = new Vector3 (newX, transform.position.y, transform.position.z);
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			float newX = transform.position.x + moveBy;
			transform.position = new Vector3 (newX, transform.position.y, transform.position.z);
		}

		if (Input.GetKey (KeyCode.UpArrow)) {
			float newY = transform.position.y + moveBy;
			transform.position = new Vector3 (transform.position.x, newY, transform.position.z);
		}

		if (Input.GetKey (KeyCode.DownArrow)) {
			float newY = transform.position.y - moveBy;
			transform.position = new Vector3 (transform.position.x, newY, transform.position.z);
		}

	}
}
