﻿#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Honours.Editor
{
    public class EditorCoroutines
    {
        public static EditorCoroutines start(IEnumerator _routine)
        {
            EditorCoroutines coroutine = new EditorCoroutines(_routine);
            coroutine.start();
            return coroutine;
        }

        readonly IEnumerator routine;
        EditorCoroutines(IEnumerator _routine)
        {
            routine = _routine;
        }

        void start()
        {
            
            EditorApplication.update += update;
        }
        public void stop()
        {
            
            EditorApplication.update -= update;
        }

        void update()
        {
          
            if (!routine.MoveNext())
            {
                stop();
            }
        }
    }
}
#endif