﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using Honours.Editor;

[ExecuteInEditMode]

public class Visualisation :MonoBehaviour {

    //Data taken down from deltaDNA
    List<string> characterPositions = new List<string>();
    List<string> deathPositions = new List<string>();
    List<string> dragonPositions = new List<string>();

    //Outputting this onto the Unity.
    List<GameObject> deathMarkers = new List<GameObject>();
    List<GameObject> lineMarkers = new List<GameObject>();
    List<GameObject> dragonMarkers = new List<GameObject>();

    struct characterMovement
    {
        public List<string> position;
        public List<string> runID;
        public List<string> sessionID;
    }

    struct dragonProximity
    {
        public List<string> startPos;
        public List<string> endPos;
        public List<string> runID;
    }

  
    private characterMovement charMovement = new characterMovement();
    private dragonProximity dragonMovement = new dragonProximity();

   

    private bool isDisplaying = false;

   

    public void Setup()
    {
        ClearEditor();
        charMovement.position = new List<string>();
        charMovement.sessionID = new List<string>();
        charMovement.runID = new List<string>();

        charMovement.position.Add("    ");



        dragonMovement.startPos = new List<string>();
        dragonMovement.endPos = new List<string>();
        dragonMovement.runID = new List<string>();

        Debug.Log("CALLED");

    }
    System.Collections.IEnumerator SetCharPosition()
    {
        GameObject parent = new GameObject();
        List<int> markerDelete = new List<int>();
        parent.name = "Character position parent";
        lineMarkers.Add(parent);

        List<Vector3> charPositionMarkers = new List<Vector3>();
        while (charMovement.position.Count > 1)
        {

            //Check against the first position in the list for character position
            string currentSession = charMovement.sessionID[4].ToString();
            string currentRun = charMovement.runID[4].ToString();


            for (int i = 1; i < charMovement.position.Count - 1; i++)
            {
                //Debug.Log("Checking against id " + i + " and character count is " + charMovement.position.Count);
                //If the session and run id match for the one we're looking for
                //add it to the list of vectors we need to process
                if (charMovement.sessionID[i].ToString().Equals(currentSession)
                    && charMovement.runID[i].ToString().Equals(currentRun))
                {
                    //Debug.Log ("Looking at position " + i + " while length is " + charMovement.position.Count);

                    string positionString = charMovement.position[i].ToString();
                    int index = positionString.LastIndexOf(',');
                    string posYString = positionString.Substring(index + 2);
                    string posXString = positionString.Substring(0, index);

                    float posX = float.Parse(posXString);
                    float posY = float.Parse(posYString);

                    Vector3 newPos = new Vector3(posX, posY, -10);
                    charPositionMarkers.Add(newPos);


                    markerDelete.Add(i);

                }


            }

            //We have finished checking through this, remove this from the list. 
            foreach (int index in markerDelete)
            {
                charMovement.position.RemoveAt(0);
                charMovement.position.RemoveAt(index);
                charMovement.sessionID.RemoveAt(index);
                charMovement.runID.RemoveAt(index);
            }

            //Create a new rendererer
            GameObject go = Resources.Load<GameObject>("position");
            go.name = "Position";
            LineRenderer lRenderer = go.GetComponent<LineRenderer>();
            lRenderer.SetVertexCount(charPositionMarkers.Count);
            GameObject lineRenderer = Instantiate(go, Vector3.zero, Quaternion.identity) as GameObject;
            //Populate the renderer with the vectors
            int lineIndex = 0;
            foreach (Vector3 vec in charPositionMarkers)
            {
                lRenderer.SetPosition(lineIndex, vec);
                lineIndex++;
            }
            lineMarkers.Add(lineRenderer);
            lineRenderer.transform.parent = parent.transform;
            //Delete everything that we have
            charPositionMarkers.RemoveRange(0, charPositionMarkers.Count);

        } //END WHILE
        yield return null;
    }

    System.Collections.IEnumerator SetDragonPosition()
    {
        GameObject dragonParent = new GameObject();
        dragonParent.name = "Dragon Movement Parent";
        dragonMarkers.Add(dragonParent);

        //Just draw something at the start positions
        for (int i = 0; i < dragonMovement.startPos.Count; i++)
        {
            //Obtain the start position
            string positionString = dragonMovement.startPos[i].ToString();
            int index = positionString.LastIndexOf(',');
            string posXString = positionString.Substring(0, index);
            float posX = float.Parse(posXString);

            Vector3 startPos = new Vector3(posX, 2, -10);

            //Obtain the end position
            positionString = dragonMovement.endPos[i].ToString();
            index = positionString.LastIndexOf(',');
            posXString = positionString.Substring(0, index);
            posX = float.Parse(posXString);

            Vector3 endPos = new Vector3(posX, 2, -10);

            GameObject go = Resources.Load<GameObject>("dragonLine");
            go.name = "DragonLineOfDeath";
            LineRenderer lRenderer = go.GetComponent<LineRenderer>();
            lRenderer.SetVertexCount(2);
            GameObject lineRenderer = Instantiate(go, Vector3.zero, Quaternion.identity) as GameObject;
            //Populate the renderer with the vectors
            int lineIndex = 0;

            lRenderer.SetPosition(0, startPos);
            lRenderer.SetPosition(1, endPos);
            lineRenderer.transform.parent = dragonParent.transform;
            dragonMarkers.Add(lineRenderer);

        }


        yield return null;
    }

    public void DisplayToWindow()
    {

        if (!isDisplaying)
        {
            isDisplaying = true;
            //----- Character positions
            EditorCoroutines.start(SetCharPosition());

            //-------------------------

            //Death positions  TODO add this to a coroutine
            GameObject parent = new GameObject();
            parent.name = "Death Markers Parent";

            deathMarkers.Add(parent);
            for (int i = 0; i < deathPositions.Count; i++)
            {
                string positionString = deathPositions[i].ToString();
                int index = positionString.LastIndexOf(',');
                string posYString = positionString.Substring(index + 2);
                string posXString = positionString.Substring(0, index);


                float posX = float.Parse(posXString);
                float posY = float.Parse(posYString);

                GameObject go = Resources.Load<GameObject>("Marker");
                go.name = "Marker " + i;
                go.transform.position = new Vector3(posX, posY, -10);
                GameObject deathMarker = Instantiate(go);
                deathMarker.transform.parent = parent.transform;
                deathMarkers.Add(deathMarker);


            }

            ChangeColour();
            ////-----------------

            EditorCoroutines.start(SetDragonPosition());

        }

    }


    private void ChangeColour()
    {
        float colorAdjuster = 0.2f;
        GameObject[] heatboxes = GameObject.FindGameObjectsWithTag("DeathMarker");

        foreach (GameObject item in heatboxes)
        {
            deathMarkers.Add(item);
            Collider2D[] hitBoxes = Physics2D.OverlapCircleAll(item.transform.position, 1f);

            foreach (Collider2D c in hitBoxes)
            {
                if (c.gameObject.tag == "DeathMarker")
                {

                    Color colorNew = item.gameObject.GetComponent<SpriteRenderer>().color;
                    colorNew.r += colorAdjuster;
                    colorNew.b -= colorAdjuster;
                    item.gameObject.GetComponent<SpriteRenderer>().color = colorNew;
                }

            }


        }

    }

    public void DisplayDeathmaps(bool _display)
    {

        foreach (GameObject g in deathMarkers)
        {
            g.SetActive(_display);
        }
    }

    public void DisplayMovement(bool _display)
    {
        foreach (GameObject g in lineMarkers)
        {
            g.SetActive(_display);
        }
    }

    public void DisplayDragon(bool _display)
    {
        foreach (GameObject g in dragonMarkers)
        {
            g.SetActive(_display);
        }
    }

    public void ClearEditor()
    {
        isDisplaying = false;
        if (deathMarkers.Count > 0)
        {
            foreach (GameObject g in deathMarkers)
            {
                DestroyImmediate(g);
            }
            deathMarkers.Clear();
            deathPositions.Clear();
        }

        if (lineMarkers.Count > 0)
        {
            foreach (GameObject g in lineMarkers)
            {
                DestroyImmediate(g);
            }
            lineMarkers.Clear();
            characterPositions.Clear();
        }

        if (dragonMarkers.Count > 0)
        {

            foreach (GameObject g in dragonMarkers)
            {
                DestroyImmediate(g);
            }
            dragonMarkers.Clear();
            dragonPositions.Clear();
        }

    }

    public void AddDeath(string _position)
    {
        deathPositions.Add(_position);
    }

    public void AddDragonMarker(string _startPos, string _endPos, string _runID)
    {
        dragonMovement.startPos.Add(_startPos);
        dragonMovement.endPos.Add(_endPos);
        dragonMovement.runID.Add(_runID);
    }

    public void AddCharacterMovement(string _pos, string _runID, string _sessionID)
    {
       
        charMovement.position.Add(_pos);
        charMovement.runID.Add(_runID);
        charMovement.sessionID.Add(_sessionID);
 
    }

    
}

#endif
