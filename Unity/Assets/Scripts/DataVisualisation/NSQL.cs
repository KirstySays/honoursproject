﻿#if UNITY_EDITOR
using System;
using UnityEngine;
using Npgsql;
using System.Collections.Generic;
using Honours.Editor;
using System.IO;

[ExecuteInEditMode]


public class NSQL : MonoBehaviour
{
    int pcCount = 0;
    int tabletCount = 0;
    private NpgsqlConnection conn;

    Visualisation visualisation = new Visualisation();
    List<int> markerDelete = new List<int>();

    private bool isDisplaying = false;

   
    public void CreateConnection()
    {
        conn = new NpgsqlConnection("Server=data.deltadna.net;Port=5432;User Id=kirstysays@gmail.com;Password=Password345;Database=abertay-student.penny-pursuit-honours; Timeout=1000;");
        conn.Open();

        if (conn.State.ToString() == "Closed")
            Debug.LogError("NSQL : Connection is closed");
        else
            Debug.Log("NSQL : Connection is open");

       
    }
    public void ReadFromDataBase()
    {
         

        // Read all rows and output the first column in each row
           

        EditorCoroutines.start(CollectData());
        

    }
    public void TextOutput()
    {
        Debug.Log("Obtaining Movement Positions");

    }

    System.Collections.IEnumerator CollectData()
    {
        NpgsqlCommand command = new NpgsqlCommand("select currentPos, runID, sessionID from events_dev", conn);
        Int32 rowsaffected;


        NpgsqlDataReader dr = command.ExecuteReader();
        int counter = 0;
        // Read all rows and output the first column in each row

       
        while (dr.Read())
        {
            if (dr[0].ToString().Contains(", ") && dr[1].ToString() != String.Empty && dr[2].ToString() != String.Empty)
            {
                //charMovement.position.Add(dr[0].ToString());
                //charMovement.runID.Add(dr[1].ToString());
                //charMovement.sessionID.Add(dr[2].ToString());

                //visualisation.AddCharacterMovement(dr[0].ToString(), dr[1].ToString(), dr[2].ToString());
            } 
 
        }

        dr.Close();

        

        command = new NpgsqlCommand("select dragonStartPos, dragonEndPos, runID from events_dev", conn);
        NpgsqlDataReader dataReader = command.ExecuteReader();

        while (dataReader.Read())
        {
            if (dataReader[0].ToString().Contains(", ") && dataReader[1].ToString().Contains(", ") && dataReader[2].ToString() != String.Empty)
            {
                //dragonMovement.startPos.Add(dataReader[0].ToString());
                //dragonMovement.endPos.Add(dataReader[1].ToString());
                //dragonMovement.runID.Add(dataReader[2].ToString());
                visualisation.AddDragonMarker(dataReader[0].ToString(), dataReader[1].ToString(), dataReader[2].ToString());
            }

        }

        dataReader.Close();

        command = new NpgsqlCommand("select deathPosition from events_dev", conn);
       
        dr = command.ExecuteReader();
        while (dr.Read())
        {
            if (dr[0].ToString().Contains(","))
                //deathPositions.Add(dr[0].ToString());
                visualisation.AddDeath(dr[0].ToString());
        }

  

        yield return new WaitForSeconds(1f);
    }
 

	
  
    public string ConnectionStatus()
    {
        if (conn != null)
            return conn.State.ToString();
        else
            return "Not found";
    }

    public void CloseConnection()
    {
        conn.Close();
    }


}
#endif