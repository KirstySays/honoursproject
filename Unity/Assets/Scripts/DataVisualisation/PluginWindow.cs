﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using UnityEditor;

public class PluginWindow : EditorWindow
{

    public Texture dotTexture;

    //game object that holds the camera
    private GameObject gameObject;

    //direct reference to the game objects camera
    private Camera cam;

    //reference to the games camera which feed we will be needing
    private Camera gameFeedCam = Camera.main;

    static PluginWindow window;


    private Rect windowRect = new Rect(100, 100, 200, 200);

    private Texture testTexture;
    private RenderTexture camTexture;

    private static NSQL nsqlInterface = new NSQL();
    private static Visualisation visualisation = new Visualisation();
    private bool showingDeathMaps = true;
    private bool previousToggle = true;
    private bool showingDragon = true;
    private bool previousDragonBool = true;

    private bool showingMovement = true;
    private bool previousMovementBool = true;

    private bool showButtons = false;


    GUIStyle guiStyle;

    [MenuItem("Visualisation/Analytics")]

    static void ShowWindow()
    {
        window =
            (PluginWindow)EditorWindow.GetWindow(typeof(PluginWindow));

        visualisation.Setup();
        
    }


    void OnGUI()
    {
          FillWindow();
    }

    void FillWindow()
    {
           
        GUILayout.Label("Connection Status : " + nsqlInterface.ConnectionStatus());

        GUILayout.Space(10);
        if (GUILayout.Button("Create Connection"))
            nsqlInterface.CreateConnection();

        if (nsqlInterface.ConnectionStatus().ToString().Equals("Open"))
        {
            showButtons = true;
        }
        else
            showButtons = false;

       

        if (showButtons)
        {
            if (GUILayout.Button("Clear"))
                visualisation.ClearEditor();

            GUILayout.Space(10);
            if (GUILayout.Button("Retrieve Data"))
                SyncData();

            if (GUILayout.Button("Display"))
                visualisation.DisplayToWindow();


            if (GUILayout.Button("TextOutput"))
                nsqlInterface.TextOutput();

             GUILayout.Space(70);
            showingDeathMaps = EditorGUI.Toggle(new Rect(0, 170, position.width, 20),
                                    "Display Deathmaps",
                                    showingDeathMaps);

            //Checks to see if we've changed the toggle so we're not calling it every damn frame.
            if(previousToggle != showingDeathMaps)
            {
                ToggleDeathmaps(showingDeathMaps);
                previousToggle = showingDeathMaps;
            }

            showingMovement = EditorGUI.Toggle(new Rect(0, 190, position.width, 20),
                                   "Display Movement",
                                   showingMovement);
        

            //Checks to see if we've changed the toggle so we're not calling it every damn frame.
            if (previousMovementBool != showingMovement)
            {
                ToggleMovement(showingMovement);
                previousMovementBool = showingMovement;
            }

            showingDragon = EditorGUI.Toggle(new Rect(0, 210, position.width, 20),
                                  "Display Dragon Proximity",
                                  showingDragon);


            //Checks to see if we've changed the toggle so we're not calling it every damn frame.
            if (previousDragonBool != showingDragon)
            {
                ToggleDragon(showingDragon);
                previousDragonBool = showingDragon;
            }

        }
    }

    private void ToggleDeathmaps(bool _display)
    {

        visualisation.DisplayDeathmaps(_display);
        
    }

    private void ToggleMovement(bool _display)
    {
        visualisation.DisplayMovement(_display);
    }


    private void ToggleDragon(bool _display)
    {
        visualisation.DisplayDragon(_display);
    }
    private void SyncData()
    {

        nsqlInterface.ReadFromDataBase();
    }
    void OnDestroy()
    {
        DestroyImmediate(gameObject);
    }
}
#endif