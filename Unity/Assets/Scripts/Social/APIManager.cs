﻿using UnityEngine;
using System.Collections;
using GameSparks.Api;
using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;

public class APIManager : MonoBehaviour {
    private GameSparks.Core.IGSPlatform platform;
	// Use this for initialization
	void Awake () {
        /*
        if (!FB.IsInitialized)
        {
            // Initalise and login to facebook
            FB.Init(FacebookLogin);
        }
        else
        {
            FacebookLogin();
        }
         */
        //GS.Reset();
        StartCoroutine(GameSparksInit());
       
	}

    public void FacebookLogin()
    {
  /*      if (!FB.IsLoggedIn)
        {
            FB.Login("email", GameSparksLogin);
        }*/
    }

    //GameSparksLogin takes FBResult from FB.Login but we don't use it 
    //for anything
/*    public void GameSparksLogin(FBResult result)
    {
        //It never hurts to double check it you are logged into Facebook                
        //before trying to log into GameSparks with Facebook
       
        if (FB.IsLoggedIn) {
            //This is the standard FacebookConnectRequest. This will                        
            //log into GameSparks with your Facebook Profile.
            new FacebookConnectRequest().SetAccessToken(FB.AccessToken).Send((response) => {
                if (response.HasErrors) {
                    Debug.Log("Something failed with connecting with Facebook");
                }
                else {
                    //Here you can display some information                                                                              //to the user that they are logged in...
                }
            });
        }
    }*/

    IEnumerator GameSparksInit() {
        while (!GS.Available) {
            yield return 0;
        }
        GS.Reconnect();
        GameSparksLogin();
    }
    public void GameSparksLogin() {
        new DeviceAuthenticationRequest().Send((response) => {
            if (response.HasErrors) {
                Debug.Log("Failed to authenticate device");
            }
            else {
                Debug.Log("Successfully authenticated device " + response.DisplayName);
                //Here you can display some information

                string displayName = "You";
#if UNITY_EDITOR
                displayName = "Tester " + System.Environment.UserName;
#endif
#if UNITY_STANDALONE_WIN
                displayName = System.Environment.UserName;
#endif
#if UNITY_ANDROID

#endif
#if UNITY_WINRT_8_1
    
#endif
                new ChangeUserDetailsRequest().SetDisplayName(displayName).Send((response2) => {
                    if (response2.HasErrors) {
                        Debug.Log("Failed to set display name");
                    }
                    else {
                        Debug.Log("Successfully set display name.");
                        //Here you can display some information
                    }
                });
            }
        });
    }
	


}
