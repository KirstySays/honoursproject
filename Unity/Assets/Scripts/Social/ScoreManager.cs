﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public struct ScoreData {
    public int distance;
    public int coinsCollected;
    public int coinsTotal;
    public float coinMultiplier;
    public int coinValue;
    public int coinScore;
    public int finalScore;
}

public class ScoreManager : MonoBehaviour {
    [SerializeField] private Distance distance;
    [SerializeField] private Collector coinCollector;

    [SerializeField] private int coinValue = 5;

    [SerializeField] private Text scoreText;

    [SerializeField] private Text hudText;

    private bool isUIUpdated;
    private NumberDisplay numberDisplay;

    public void Start()
    {
        isUIUpdated = false;
        numberDisplay = GetComponent<NumberDisplay>();
    }

    void OnEnable() {
        Messenger.AddListener("ResetAll", Reset);
    }

    void OnDisable() {
        Messenger.RemoveListener("ResetAll", Reset);
    }

    public void Update() {
        if (!isUIUpdated) {
            UpdateScore();
        }
    }
 
    //Used for updating the HUD while in game.
    public ScoreData UpdateScore()
    {
        ScoreData data = new ScoreData(); 
       
        data.distance = Mathf.RoundToInt(distance.GetDistanceTravelled());
        data.coinsCollected = coinCollector.GetCollectedCount();
        data.coinsTotal = data.coinsCollected;
        if (data.coinsTotal > 0)
        {
            data.coinMultiplier = data.coinsCollected / data.coinsTotal;
        }
        else
        {
            data.coinMultiplier = 0.0f;
        }
        data.coinValue = coinValue;
        data.coinScore = Mathf.RoundToInt(data.coinsCollected * data.coinMultiplier * data.coinValue);
        data.finalScore = data.distance + data.coinScore;


        numberDisplay.UpdateHUD(data.finalScore);

        return data;
    }

    public int GetCollectedCoins()
    {
        return coinCollector.GetCollectedCount();
    }

    public ScoreData GenerateScore() {
        ScoreData data = new ScoreData();

        Debug.Log("We're generating the score");

        data.distance = Mathf.RoundToInt(distance.GetDistanceTravelled());
        data.coinsCollected = coinCollector.GetCollectedCount();
        data.coinsTotal = data.coinsCollected;
        if (data.coinsTotal > 0) {
            data.coinMultiplier = data.coinsCollected / data.coinsTotal;
        }
        else {
            data.coinMultiplier = 0.0f;
        }
        data.coinValue = coinValue;
        data.coinScore = Mathf.RoundToInt(data.coinsCollected * data.coinMultiplier * data.coinValue);
        data.finalScore = data.distance + data.coinScore;

        if(!isUIUpdated)
        {
            Debug.Log("Updating score UI");
            numberDisplay.ProcessScore(data.finalScore);
            //scoreText.text = data.finalScore.ToString();
            isUIUpdated = true;
        }
       
        
        Debug.Log("Score Data: " + "Distance: " + data.distance + "...Coins Collected: " + data.coinsCollected + "...Coin Score: " + data.coinScore + "...Final Score: " + data.finalScore);
        return data;
    }

    public void Reset()
    {
        isUIUpdated = false;
        distance.ResetDistance();
        coinCollector.ResetCoins();

        numberDisplay.Reset();
        
    }
}
