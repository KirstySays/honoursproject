﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

public class Leaderboard : MonoBehaviour {

    public GameObject leaderboardEntryPrefab;
    public List<GameObject> entries = new List<GameObject>();

	// Use this for initialization
	void Start () {
        StartCoroutine(Test());
	}

    IEnumerator Test() {
        for (float i = 0; i < 10.0f; i += Time.deltaTime) {
            yield return 0;
        }
        PostHighscore(123);
        GetLeaderboard();
    }

    public void GetLeaderboard()
    {
        // Destroy existing objects
        for (int i = 0; i < entries.Count; i++)
        {
            Destroy(entries[i]);
        }
        entries.Clear();

        
        if (GameSparks.Core.GS.Authenticated) {
            new LeaderboardDataRequest_LEADERBOARD().SetEntryCount(10).Send((response) => {
                foreach (var entry in response.Data) {
                    GameObject go = (GameObject)Instantiate(leaderboardEntryPrefab);
                    var leaderboardEntry = go.GetComponent<LeaderboardEntry>();
                    leaderboardEntry.rank = entry.Rank.ToString();
                    leaderboardEntry.username = entry.UserName.ToString();
                    leaderboardEntry.score = entry.GetNumberValue("SCORE").ToString();
                    entries.Add(go);
                }
            });
        }
        else {
            int highscore = PlayerPrefs.GetInt("HIGH_SCORE");
            GameObject go = (GameObject)Instantiate(leaderboardEntryPrefab);
            var leaderboardEntry = go.GetComponent<LeaderboardEntry>();
            leaderboardEntry.rank = "1";
            leaderboardEntry.username = "You";
            leaderboardEntry.score = highscore.ToString();
            entries.Add(go);
            Debug.Log("High score: " + leaderboardEntry.score);
        }

    }

    public void PostHighscore(int score)
    {
        if (GameSparks.Core.GS.Authenticated) {
            new LogEventRequest_HIGH_SCORE().Set_SCORE(score).Send((response) => {
                if (response.HasErrors) {
                    Debug.Log("failed to post score: " + response.Errors);
                }
                else {
                    Debug.Log("Posted score");
                }
            });
        }
        else {
            int highscore = PlayerPrefs.GetInt("HIGH_SCORE");
            if (highscore < score) {
                Debug.Log("New high score: " + score);
                PlayerPrefs.SetInt("HIGH_SCORE", score);
            }
        }
    } 
	
	// Update is called once per frame
	void Update () {
        
	}
}
