﻿using UnityEngine;
using System.Collections;

public class CollisionDetector : MonoBehaviour {
    private DeltaDNAEventBuilder DDeventBuilder;

	// Use this for initialization
	void Start () {
        DDeventBuilder = new DeltaDNAEventBuilder();
	}
	
	void OnCollisionEnter2D(Collision2D collider)
    {
        Debug.Log("COLLISIONG WITH PLAYER");
        if (collider.gameObject.name == "Player")
        {
            string pos = " " + transform.position.x + ", " + transform.position.y;
            DDeventBuilder.RecordCollisionWithObstacle(pos);

        }
    }
}
