﻿using UnityEngine;
using System.Collections;

public class SectionEnd : MonoBehaviour {
    public enum sectionDifficulty { EASY, EASYMED, MED, MEDHARD, HARD, DEFAULT }
    private DeltaDNAEventBuilder DDeventBuilder;

    [SerializeField]
    private sectionDifficulty thisDifficulty = sectionDifficulty.DEFAULT;

	// Use this for initialization
	void Start () {
        DDeventBuilder = new DeltaDNAEventBuilder();
	}
	
	void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player")
        {
            //Let ddna know we have completed this section. 
            DDeventBuilder.RecordSectionComplete(thisDifficulty.ToString());
        }
    }
}
