﻿using UnityEngine;
using System.Runtime.InteropServices;

public class TestDLL : MonoBehaviour {

	[DllImport("UnityPlugin 1", EntryPoint = "TestSort")]
	public static extern void TestSort (int[]a, int length);

	public int [] a ;
	//PluginWindow pluginWindow;
	// Use this for initialization
	void Start () {

		//pluginWindow = new PluginWindow ();
		a = new int[]{5,8,19,5,9,10,400,12};


		TestSort (a, a.Length);

		for (int i = 0; i < a.Length; i++) {
			Debug.Log ("a " + i + " = " + a [i]);
		}


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
