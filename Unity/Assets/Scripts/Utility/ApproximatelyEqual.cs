﻿using UnityEngine;

namespace Utility {
    public static class ApproximatelyEqualWrapper {
        
        /// <summary>
        /// Determines whether Vector3s A and B are equal to one another within the given margin of error E
        /// </summary>
        /// <param name="a">A</param>
        /// <param name="b">B</param>
        /// <param name="e">E</param>
        /// <returns>True or False</returns>
        public static bool ApproximatelyEqual(this Vector3 a, Vector3 b, float e) {
            return (Mathf.Abs(a.x - b.x) <= e && Mathf.Abs(a.y - b.y) <= e && Mathf.Abs(a.z - b.z) <= e);
        }

        /// <summary>
        /// Determines whether Vector2s A and B are equal to one another within the given margin of error E
        /// </summary>
        /// <param name="a">A</param>
        /// <param name="b">B</param>
        /// <param name="e">E</param>
        /// <returns>True or False</returns>
        public static bool ApproximatelyEqual(this Vector2 a, Vector2 b, float e) {
            return (Mathf.Abs(a.x - b.x) <= e && Mathf.Abs(a.y - b.y) <= e);
        }

        /// <summary>
        /// Determines whether floats A and B are equal to one another within the given margin of error E
        /// </summary>
        /// <param name="a">A</param>
        /// <param name="b">B</param>
        /// <param name="e">E</param>
        /// <returns>True or False</returns>
        public static bool ApproximatelyEqual(this float a, float b, float e) {
            return (Mathf.Abs(a - b) <= e);
        }
    }

};
