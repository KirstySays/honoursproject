﻿/*
 * Author : Kirsty Fraser
 * Date created : 4.7.15
 * Last amended : n.A
 * 
 * Version 1.0
 * 
 * Purpose : To take in a score in string format from the ScoreManager and use this information to
 * display the score using sprites rather than raw text
 * 
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NumberDisplay : MonoBehaviour {

    [SerializeField]
    private Sprite[] spriteNumbers;                  //Holds all of our images in one data structure.

    [SerializeField]
    private GameObject parentObject;                    //gameobject that will be holding all of the number sprites in it. 

    [SerializeField]
    private GameObject scoreHud;

    [SerializeField]
    private GameObject prefab;

    [SerializeField] [ReadOnly]
    private int inGamePadding = 30;

    [SerializeField]
    private int endScorePadding = 75;                 //Gap between each number

    private ArrayList displayList;              //Holds the numbers that we're needing to display.

    private int currentArrayIndex;
    private int currentScoreLength;
    int offset; 

    void Awake()
    {
        float width = Screen.width;
        float hudPadding = 0;

        if (width < 1000)
        {
            hudPadding = width / 25;
        }

        else if (width > 1000)
        {
            hudPadding = width / 50;
        }

        inGamePadding = (int)hudPadding;
        currentScoreLength = 1;
        offset = inGamePadding;

    }

    public void UpdateHUD(int _updatedScore)
    {
        string scoreString = _updatedScore.ToString();

        char[] scoreArray = scoreString.ToCharArray();

     

        int i = 0;
        foreach(Transform child in scoreHud.transform)
        {
            //Debug.Log("Updating the in game score");
            Debug.Log(i);
            int arrayIndex = scoreArray[i] - 48;
            if(currentArrayIndex != arrayIndex)
            {
                //Change the sprite
                Sprite newNumber = spriteNumbers[arrayIndex];

                child.GetComponent<Image>().sprite = newNumber;

                currentArrayIndex = arrayIndex;

            }


            if (scoreArray.Length > currentScoreLength) 
           {
               //Debug.Log("Adding another child");
               //Spawn in another child using the offset required
               float posX = scoreHud.transform.position.x + offset;
               Vector3 spawnPos = new Vector3(posX, scoreHud.transform.position.y, scoreHud.transform.position.z);
               GameObject childObject = Instantiate(prefab, spawnPos, Quaternion.identity) as GameObject;
               childObject.transform.SetParent(scoreHud.transform);
               childObject.transform.localPosition = new Vector3(childObject.transform.localPosition.x + offset, childObject.transform.localPosition.y, childObject.transform.localPosition.z);
               childObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

               childObject.GetComponent<Image>().sprite = spriteNumbers[0];
               offset += inGamePadding;
               currentScoreLength = scoreArray.Length;
           }
            
            if (i < scoreArray.Length)
                i++;
            else 
                break;
        }
   

    }
    public void ProcessScore(int _newScore)
    {
        string scoreString = _newScore.ToString();

        char[] scoreArray = scoreString.ToCharArray();
        
        //Delete any children that we may already have.
        foreach(Transform child in parentObject.transform)
        {
            DestroyObject(child.gameObject); 
        }

       int offset = 0;
        for (int i = 0; i < scoreArray.Length; i++ )
        {
            //Get the int contained in the score array
            int arrayIndex = scoreArray[i] - 48;

            //Create a new child object to the parent to hold the number in
            float posX = parentObject.transform.position.x + offset;
            Vector3 spawnPos = new Vector3(posX, parentObject.transform.position.y, parentObject.transform.position.z);
            GameObject childObject = Instantiate(prefab, spawnPos, Quaternion.identity) as GameObject;
            childObject.transform.SetParent(parentObject.transform);
            childObject.transform.localPosition = new Vector3(childObject.transform.localPosition.x + offset, childObject.transform.localPosition.y, childObject.transform.localPosition.z);
            childObject.transform.localScale = new Vector3(2, 2, 2);
         
           

            //Obtain the sprite required
            Sprite newNumber = spriteNumbers[arrayIndex];

            //Add it to the image component of the newly created gameobject
            childObject.GetComponent<Image>().sprite = newNumber;

            offset += endScorePadding;              //TODO create this to be serialisable so Akos don't kill you

        }
    }


    public void Reset()
    {
        offset = inGamePadding;
        currentScoreLength = 1;

        for (int i =  1; i < scoreHud.transform.childCount; i++)
        {
            Destroy(scoreHud.transform.GetChild(i).gameObject);
        }


    }
  
}
