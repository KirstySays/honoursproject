﻿/*
 * Author : Kirsty Fraser
 * Date created : 21.6.15
 * Last amended : n.a
 * 
 * Version 1.0
 * 
 * Purpose : Cleanup any gameobjects which have gone off screen.  
 * 
 */

using UnityEngine;
using System.Collections;

public class MemoryCleanUp : MonoBehaviour {

	void OnBecameInvisible()
	{
		Destroy (this.gameObject);
	}
}
