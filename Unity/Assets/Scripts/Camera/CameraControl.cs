﻿/*
 * Kirsty Fraser
 * Created 27.5.15
 * Last amended 27.5.15
 * Version 1.0
 * Purpose : To be used to activate pause points in the game where appropriate e.g. on screens where text need to be read.
 * 			 Used in scene, the user can place 'pause' game objects in the scene where they wish the camera to be stopped.
 * 
 * 
 */ 


using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	[SerializeField]
	private float pauseTime;

	[SerializeField]
	private bool isStoppingTrigger;

	[SerializeField]
	private bool isSpeedTrigger;

	[SerializeField]
	private float cameraSpeed;

    [SerializeField]
    private float delay;                                    //Any time delay for the animation of the UI element

    [SerializeField]
    private Transform settingsMoveTo;                               //How much to move the UI element by

    [SerializeField]
    private Transform menuMoveTo;
    
    [SerializeField]
    private Transform scoresMoveTo;
		
	[SerializeField]
	private Transform creditsMoveTo;

    [SerializeField]
    private Transform gameMoveTo;

    [SerializeField]
    private EaseType eEaseType;                             //Choosing which kind of ease type to use for the UI element

    [SerializeField]
    private LoopType eLoopType;                             //Choosing which kind of loop type for using on the UI element

    [SerializeField]
    private float timeToComplete;                           //Full time taken to complete the animation.

	private ScrollingCamera mScrollingCamera;

    [SerializeField] private InputModule gameInput;
    [SerializeField]
    private SectionAssembler sectionAssembler;
    

	//DEBUG
	//public GameObject mProcSection;

	//private LevelLoader mLevelLoader;

	// Use this for initialization
	void Start () {

		//mLevelLoader = mProcSection.GetComponent<LevelLoader> ();
        gameInput.DisableInput();
	
	}

    void OnTriggerEnter2D(Collider2D collider) {

    }

    public void MoveCamera(string _gameState)
    {
        sectionAssembler.isBackgroundShow = true;
		switch (_gameState) {
		case "Settings":
			{
                gameInput.DisableInput();
				iTween.MoveTo (this.gameObject, iTween.Hash ("x", settingsMoveTo.position.x, "y", settingsMoveTo.position.y, "z", settingsMoveTo.position.z, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString (), "easeType", eEaseType.ToString ()));
				break;
			}

		case "MainMenu":
			{
                gameInput.DisableInput();
				Debug.Log ("Calling this main menu btn");
				iTween.MoveTo (this.gameObject, iTween.Hash ("x", menuMoveTo.position.x, "y", menuMoveTo.position.y, "z", menuMoveTo.position.z, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString (), "easeType", eEaseType.ToString ()));
				break;
			}

		case "Scores":
			{
                gameInput.DisableInput();
				iTween.MoveTo (this.gameObject, iTween.Hash ("x", scoresMoveTo.position.x, "y", scoresMoveTo.position.y, "z", scoresMoveTo.position.z, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString (), "easeType", eEaseType.ToString ()));
				break;
			}

		case "Play":
			{
				iTween.MoveTo (this.gameObject, iTween.Hash ("x", gameMoveTo.position.x, "y", gameMoveTo.position.y, "z", gameMoveTo.position.z, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString (), "easeType", eEaseType.ToString ()));
                gameInput.EnableInput();
                sectionAssembler.isBackgroundShow = false;
                //mainCamera.GetComponent<CameraFollow> ().StartCamera ();
				break;
			}
		case "Credits":
			{
                gameInput.DisableInput();
				iTween.MoveTo (this.gameObject, iTween.Hash ("x", creditsMoveTo.position.x, "y", creditsMoveTo.position.y, "z", creditsMoveTo.position.z, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString (), "easeType", eEaseType.ToString ()));
				break;
			}
	       
		}
	}
   
}
