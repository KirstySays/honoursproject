﻿using UnityEngine;
using System.Collections;

public class CameraGenerate : MonoBehaviour {

	[SerializeField] 
	private GameObject proceduralSection;

	void OnTriggerExit2D(Collider2D collider) 
	{ 
		Destroy (collider.gameObject);  
	} 
	void OnTriggerEnter2D(Collider2D collider) 
	{ 
		if (collider.gameObject.tag == "procedural") 
			//Need to load up another level 
			proceduralSection.GetComponent<LevelLoader> ().LoadNewLevel (); 
	}
}
