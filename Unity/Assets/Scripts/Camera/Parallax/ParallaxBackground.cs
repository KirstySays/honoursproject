﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class ParallaxBackground : MonoBehaviour {
    public ParallaxCamera parallaxCamera;
    public List<ParallaxLayer> parallaxLayers = new List<ParallaxLayer>();

    void Start() {
        if (parallaxCamera == null) {
            parallaxCamera = Camera.main.GetComponent<ParallaxCamera>();
        }

        if (parallaxCamera != null) {
            parallaxCamera.onCameraTranslate += Move;
        }

        

        SetLayers();
    }

    void SetLayers() {
        parallaxLayers.Clear();
        var layers = gameObject.GetComponentsInChildren<ParallaxLayer>();
        for (int i =0; i < layers.Length; i++) {
            var layer = layers[i];
            if (layer != null) {
                parallaxLayers.Add(layer);
            }
        }
    }

    void Update() {
        SetLayers();
    }


    void Move(float delta) {
        foreach (ParallaxLayer layer in parallaxLayers) {
            layer.Move(delta);
        }
    }
}
