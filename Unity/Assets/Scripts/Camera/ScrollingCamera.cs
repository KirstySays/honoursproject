﻿/*
 * Kirsty Fraser
 * Created 20.5.15
 * Last amended 24.5.15
 * Version 1.1
 * Purpose : Used on the camera object to create a 'scrolling' camera for use within a 2D game.
 * 			Requires a 2D box collider attached to the camera to be used for deleting the levels as they roll 
 * 			off camera.
 * 
 * 
 */ 
using UnityEngine;
using System.Collections;

public class ScrollingCamera : MonoBehaviour {
	
	[SerializeField]
	private float cameraSpeed;						//Can be accessed on editor side to be used to control speed of camera.

	[SerializeField]
	private GameObject proceduralSection;

	private bool isCameraPaused;

	void Start()
	{
		//Check to see if 2DCollider is attached to the camera.
		if (gameObject.GetComponent<BoxCollider2D> () == null)
		{
			Debug.LogError("Please attach BoxCollider2D to Camera.");
		}

		//Check if we have a reference to the procedrual section
		if (proceduralSection == null) {
			Debug.LogError("Please connect a procedruals ection which is being used in the scene to the Scrolling Camera script");
		}

		isCameraPaused = false;
	}

	// Update is called once per frame
	void Update ()
	{
		if (!isCameraPaused) {
			float currentX = gameObject.transform.position.x;
			float newX = currentX + cameraSpeed / 10;			//Divide by ten so we get a sensible speed.

			Vector3 newPosition = new Vector3 (newX, gameObject.transform.position.y, -10);
			gameObject.transform.position = newPosition;
		}

		if(Input.GetKeyDown(KeyCode.Space))
		   isCameraPaused = false;
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		  if(collider.tag != "ignore")
			Destroy (collider.gameObject);

          if (collider.tag == "transitional")
          {
              Debug.Log("Transitional piece");

              proceduralSection.transform.parent.GetComponent<ProceduralManager>().SwapSections();
          }
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		Debug.Log ("Calling the enter");
		if (collider.gameObject.tag == "procedural")
			//Need to load up another level
			proceduralSection.GetComponent<LevelLoader> ().LoadNewLevel ();

       
                      //Horrible and hacky I know! D: SORT IT OUT ASAP.
	}

	//Timed pause for the use in tutorial levels 
	public void PauseCamera(float _pauseTime)
	{
		Debug.Log ("IEnumerator pauing camera " + _pauseTime);
		isCameraPaused = true;
		StartCoroutine (PauseBy (_pauseTime));
	}

	//Pause which is indefinite until the user hits a menu button to continue
	public void PauseCamera()
	{
		isCameraPaused = true;
	}

	public void UnpauseCamera()
	{
		isCameraPaused = false;
	}

	public void ChangeSpeed(float _cameraSpeed)
	{
		Debug.Log ("Changing Speed " + _cameraSpeed);
		cameraSpeed = +_cameraSpeed;
	}

	//Waits the number of seconds which is passed in and then starts the moving camera again.
	IEnumerator PauseBy(float _pauseTime) {
		yield return new WaitForSeconds (_pauseTime);
		UnpauseCamera ();
	}

}
