﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    GameObject player;

    [SerializeField]
    float distance = 4.0f;

    [SerializeField]
    float stiffness = 0.75f;
	
    [SerializeField]
    bool isCameraMoving;

	// Use this for initialization
	void Start () 
	{

        isCameraMoving = true;
		//Check if we have a reference to the procedrual section
	/*	if (proceduralSections == null) {
			Debug.LogError("Please connect a procedruals ection which is being used in the scene to the Scrolling Camera script");
		}*/

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if(isCameraMoving)
        {
            Vector3 currentPos = transform.position;
            float actualZ = currentPos.z;
            Vector3 playerPos = player.transform.position;

            //Vector3 desiredPos = player.transform.position;
		    //MAGIC NUMBERS MAAAAN, need starting Y position of camera
            Vector3 desiredPos = new Vector3(playerPos.x, transform.position.y, playerPos.z);
            desiredPos.x += distance;
            Vector3 intermediatePos = Vector3.Lerp(currentPos, desiredPos, Time.deltaTime*stiffness);
            intermediatePos.z =  actualZ;
            transform.position = intermediatePos;

        }
       

	}

    //(Added by Kirsty) to cope with the stopping and movement of a camera for control for the menus
    public void StopCamera()
    {
        isCameraMoving = false;
    }

    public void StartCamera()
    {
        isCameraMoving = true;
    }


	//(Added by Kirsty) Will be used to delete procedural prefabs after they're out of shot. Uses the layer 'LevelLoader' for checking.
	void OnTriggerExit2D(Collider2D collider)
	{
		//if(collider.tag != "ignore")
		Destroy (collider.gameObject);	

      
	}

    ////(Added by Kirsty) Will be used to create procedural prefabs after they're out of shot. Uses the tag 'procedrual' to check if we've hit a procedrual level.
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "transitional")
        {
            //Let the procedural manager know
            collider.transform.parent.GetComponent<ProceduralManager>().SwapSections();

        }
    }
}
