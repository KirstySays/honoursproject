﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour
{
    private static float shakeTimer;
    private static float shakeAmount;
    private float initialZ;

    // Use this for initialization
    void Start()
    {
        initialZ = gameObject.transform.localPosition.z;
    }

    public void ShakeNow(float time, float intensity)
    {
       // Debug.Log("Shake the screen");
        Random.seed = (int)(time * intensity);
        shakeTimer = time;
        shakeAmount = intensity;
    }

    // Update is called once per frame
    void Update()
    {
        if (shakeTimer > 0.0f)
        {
            Random.seed = (int)(shakeTimer * shakeAmount * 100.0f);
            Vector3 shake = Random.insideUnitSphere * shakeAmount;
            shake.z = initialZ;
            gameObject.transform.localPosition = shake;
            shakeAmount -= (Time.deltaTime * shakeAmount) / shakeTimer;
            shakeTimer -= Time.deltaTime;
        }
        else
        {
            shakeTimer = 0.0f;
        }
    }
}