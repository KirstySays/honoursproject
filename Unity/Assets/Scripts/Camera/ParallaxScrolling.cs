﻿using UnityEngine;
using System.Collections;

public class ParallaxScrolling : MonoBehaviour {

	public Transform [] parallaxObjects;					//Holds the objects which parallax scrolling will be applied to
	private float[] parallaxScales;							//Proportion of the camera's movement to move the backgrounds by.
	public float parallaxAmount = 1f;						//How smooth the parrlax effect will be. Must be above 0.

	private Transform cam;								//Reference to the main camera's transform.
	private Vector3 previousCamPosition;					//Stores the position of the camera in the previous frame.

	//Called before Start(). 
	void Awake()
	{
		//Reference to the camera.
		cam = Camera.main.transform;
	}

	// Use this for initialization
	void Start () 
	{
		//Store previous frame had the current frames camera position
		previousCamPosition = cam.position;

		//Loop through backgrounds list and assign each element a parallax scale
		parallaxScales = new float[parallaxObjects.Length];
		for (int i = 0; i < parallaxObjects.Length; i++)
		{
			parallaxScales[i] = parallaxObjects[i].position.z*-1;
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		//For each background
		for (int i =0; i < parallaxObjects.Length; i++) {
			//the parallax is the opposite of the camera movement because the previous frame multiplied by the scale
			float parallax = (previousCamPosition.x - cam.position.x) * parallaxScales[i];
			//float parallaxY = (previousCamPosition.y - cam.position.y) * parallaxScales[i];

			// Set a target x position which is the current position plus the parallax
			float backgroundTargetPosX = parallaxObjects[i].position.x + parallax;
			//float backgroundTargetPosY = parallaxObjects[i].position.y + parallaxY;

			//Create a target position which is the backgrounds current position with it's target position
			//TODO can add to the Y as well for height.
			Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, parallaxObjects[i].position.y, parallaxObjects[i].position.z);

			//Fade between current position and the target position using lerp
			parallaxObjects[i].position = Vector3.Lerp(parallaxObjects[i].position, backgroundTargetPos, parallaxAmount * Time.deltaTime);
		}

		//Set the previosCamPos to the camera's position at the end of the frame
		previousCamPosition = cam.position;
	}
}
