﻿using UnityEngine;

/// <summary>
/// Custom Attribute that shows the value of a property in the Unity Editor without allowing it to be modified
/// </summary>
public class ReadOnlyAttribute : PropertyAttribute {

}
