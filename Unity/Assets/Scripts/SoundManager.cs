﻿/*
 * Author : Kirsty Fraser
 * Date created : 27.6.15
 * Last amended : n.A
 * 
 * Version 1.0
 * 
 * Purpose : Control the sound within the game, whether the sounds are playing
 * 
 * 
 */

using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {


	[SerializeField]
	private AudioClip buttonClick;

	[SerializeField]
	private AudioSource bgMusic;

	private AudioSource SFXSource;

	private bool isSFXOn;

	private bool isMusicOn;


	// Use this for initialization
	void Start () 
	{
		isSFXOn = true;
		isMusicOn = true;

		bgMusic.Play ();
	}
	
	public void TurnSFXOff()
	{
        Debug.Log("SFX is off");
		isSFXOn = false;
	}

	public void TurnSFXOn()
	{
        Debug.Log("SFX is on");
		isSFXOn = true;
	}

	public void TurnMusicOff()
	{
        Debug.Log("Music is off");
		isMusicOn = false;
        bgMusic.Pause();
	}
	
	public void TurnMusicOn()
	{
        Debug.Log("Music is on");
		isMusicOn = true;
        bgMusic.Play();

	}

    public bool IsMusicOn()
    {
        return isMusicOn;
    }

    public bool IsSFXOn()
    {
        return isSFXOn;
    }

	public void PlayButtonClick()
	{
		SFXSource.PlayOneShot (buttonClick, 1.0f);
	}

	public void PlayBGMusic()
	{
		bgMusic.Play ();
	}
}
