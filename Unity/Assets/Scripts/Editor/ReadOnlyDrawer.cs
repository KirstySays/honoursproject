﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom Property drawer to handle drawing of ReadOnlyAttribute in the Unity Editor
/// </summary>
[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer {

    public override float GetPropertyHeight(SerializedProperty property,GUIContent label) {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
     {
         string valueStr;
 
         switch (prop.propertyType)
         {
             case SerializedPropertyType.Integer:
                 valueStr = prop.intValue.ToString();
                 break;
             case SerializedPropertyType.Boolean:
                 valueStr = prop.boolValue.ToString();
                 break;
             case SerializedPropertyType.Float:
                 valueStr = prop.floatValue.ToString("0.00000");
                 break;
             case SerializedPropertyType.String:
                 valueStr = prop.stringValue;
                 break;
             default:
                 valueStr = "(not supported)";
                 break;
         }
 
         EditorGUI.LabelField(position,label.text, valueStr);
     }
}


