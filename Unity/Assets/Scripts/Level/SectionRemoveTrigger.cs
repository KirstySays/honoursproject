﻿using UnityEngine;
using System.Collections;

public class SectionRemoveTrigger : MonoBehaviour {

    [SerializeField]  private string removeTag = "procedural";
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == removeTag) {
            collider.transform.parent.GetComponent<SectionManager>().RemoveSection(collider.GetComponent<Section>()); 
        }
    }
}
