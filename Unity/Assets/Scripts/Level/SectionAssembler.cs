﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public enum AreaType { Corridor, Lava };
public class SectionAssembler : MonoBehaviour {

    private List<Section> corridorSections = new List<Section>();
    private List<Section> lavaSections = new List<Section>();

    [Header("Transitions")]
    [SerializeField] private Section corridorToLavaTransition;
    [SerializeField] private Section lavaToCorridorTransition;
	[SerializeField] private Section tutorialSection;

    [SerializeField] private int minimumSectionCount = 3;
    [SerializeField] private int maximumSectionCount = 10;

    [SerializeField] [ReadOnly] private AreaType currentArea;
    [SerializeField] [ReadOnly] private int currentCount = 0;

    [SerializeField] private SectionManager sectionManager;

    private List<Section> currentSections;
    private Section currentTransition;

    private bool nextSectionNeeded = false;
    public bool isBackgroundShow = true;
	private bool shownTutorial = false;

	// Use this for initialization
	void Awake () {
        LoadSections();
        currentSections = corridorSections;
        currentTransition = corridorToLavaTransition;
        currentArea = AreaType.Corridor;
        currentCount = 0;
    }

    void OnEnable() {
        Messenger.AddListener("ResetAll", Reset);
    }

    void OnDisable() {
        Messenger.RemoveListener("ResetAll", Reset);
    }
	
	// Update is called once per frame
	void Update () {
        if (sectionManager.GetCurrentActiveSectionCount() < sectionManager.numberOfActiveSections && !isBackgroundShow) {
            nextSectionNeeded = true;
        }
        else if (sectionManager.GetCurrentActiveSectionCount() < 5 && isBackgroundShow) {
            nextSectionNeeded = true;
        }

        if (nextSectionNeeded) {
            if (!isBackgroundShow) {
                ChooseNextPrefab(currentSections, currentSections.Count);
            }
            else {
                AddSection(currentSections.First());
            }

            nextSectionNeeded = false;
        }
	}

    void LoadSections() {
        foreach (GameObject go in Resources.LoadAll("CorriderScene")) {
            if (go.tag == "procedural") {
                corridorSections.Add(go.GetComponent<Section>());
            }
        }

        foreach (GameObject go in Resources.LoadAll("LavaScene")) {
            if (go.tag == "procedural") {
                lavaSections.Add(go.GetComponent<Section>());
            }
        }
    }

    // Kirsty's logic repurposed for section based system
    private void ChooseNextPrefab(List<Section> _contentList, int _numOfLevels) {
        int randIndex = Random.Range(0, _contentList.Count);
        Section nextSection = null;

		if (!shownTutorial) {
			nextSection = tutorialSection;
			shownTutorial = true;
		}

        else if (currentCount > maximumSectionCount) {
            nextSection = currentTransition;
            if (currentArea == AreaType.Corridor) {
                currentSections = lavaSections;
                currentTransition = lavaToCorridorTransition;
                currentArea = AreaType.Lava;
            }
            else {
                currentSections = corridorSections;
                currentTransition = corridorToLavaTransition;
                currentArea = AreaType.Corridor;
            }
            currentCount = 0;
        }
        else {
            nextSection = _contentList[randIndex];
        }
        currentCount++;
        AddSection(nextSection);
    }

    private void AddSection(Section nextSection) {
        var section = (Section)Instantiate(nextSection);
        section.transform.position = transform.position;
        section.transform.parent = sectionManager.transform;
        sectionManager.AddSection(section);
    }

    public void Reset() {
        Debug.Log("Resetting Section Assembler");
        currentCount = 0;
        currentArea = AreaType.Corridor;
        currentSections = corridorSections;
        currentTransition = corridorToLavaTransition;
        //AddSection(currentSections.First());
    }
}
