﻿/*
 * Kirsty Fraser
 * Created 11.6.15
 * Last amended 11.6.15
 * Version 1.0
 * 
 * Purpose : To be used to control tell the Procedural Manager that we need to move the procedrual section that we've just finsihed with
 * 
 * Requires :
 * N.A
 */
using UnityEngine;
using System.Collections;

public class Transition : MonoBehaviour {
    void OnTriggerExit2D(Collider2D collider)
    {
        Debug.Log("Trigger being called");
        transform.parent.GetComponent<ProceduralManager>().SwapSections();
    }
}
