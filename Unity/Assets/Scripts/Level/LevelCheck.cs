﻿using UnityEngine;
using System.Collections;

public class LevelCheck : MonoBehaviour {

    //TRYING THIS FOR DEBUG REASONS TO SEE IF THIS WILL WORK.
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "transitional")
        {
            //Let the procedural manager know
            collider.transform.parent.GetComponent<ProceduralManager>().StartSpawningInNextArea();

        }
    }
}
