﻿/*
 * Kirsty Fraser
 * Created 11.6.15
 * Last amended 19.6.15
 * Version 1.2
 * 
 * Purpose : To be used to control the procedural sections and which ones should be aligned and active.
 * 
 * Requires :
 * N.A
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProceduralManager : MonoBehaviour {

    [SerializeField]
    private List<GameObject> proceduralSections;                //A list of the procedural sections which are used within the game

    [SerializeField]
    private Vector3 startingPosition;                         //Defines the starting position for the first procedural level.

    [SerializeField]
    private GameObject transitionPiece;                         //Allocates which piece of art we're using for the transitions.

	[SerializeField]
	private float multiplier;								//Multiplier for the transitional pieces

    private int[] proceduralAreaLengths;                    //Keeps a hold of the lengths of each area.

    private int currentProceduralSection;                       //Keeps track of which element in the array we are using currently.

    private GameObject newTransition;                       //Keeps a hold of the last transitional piece we placed so we can use this for aligning areas.
    private int nextProceduralSection;
    private int futureSection;

	// Use this for initialization
	void Start () {

        //Check if we've entered information into this on the editor.
	    if(proceduralSections.Count == 0)
        {
            Debug.LogError("Please fill out the procedrualSections on the ProceduralManager.");

        }

		if (proceduralSections.Count == 1) 
		{
			currentProceduralSection = 0;

			//We only have one piece to worry about
			proceduralSections[currentProceduralSection].transform.position= startingPosition;
			proceduralSections[currentProceduralSection].GetComponent<LevelLoader>().ResetSection();

			//Assign the transitional piece
			float radius = transitionPiece.GetComponent<SpriteRenderer>().bounds.size.x;
			radius = radius * 0.5f;
			
			
			float newX = startingPosition.x + proceduralSections[currentProceduralSection].GetComponent<LevelLoader>().GetLength()*multiplier + radius;

			newTransition = (GameObject) Instantiate(transitionPiece,new Vector3(newX, startingPosition.y,0.0f), Quaternion.identity );
			newTransition.transform.parent = gameObject.transform;
			
			
		}
		
		if (proceduralSections.Count >= 2) {                    //Then we have more than one areas
			//Assign the first proc section to start at the position provided
			currentProceduralSection = 0;
			nextProceduralSection = Random.Range (1, proceduralSections.Count);              //Don't include 0 as this is the current proc section.

			//Need to place the rest of the sections. 
			if (nextProceduralSection == 1)
				futureSection = 2;
			else
				futureSection = 1;


			//Move the first section to be the same as the starting position and then start the first section off 
			float startX = startingPosition.x + proceduralSections [currentProceduralSection].GetComponent<LevelLoader> ().GetLength () * 4f;
            proceduralSections[currentProceduralSection].transform.position = new Vector3(startingPosition.x, startingPosition.y, startingPosition.z);
			proceduralSections [currentProceduralSection].GetComponent<LevelLoader> ().ResetSection ();



			//Assign the transitional piece
			float radius = transitionPiece.GetComponent<SpriteRenderer> ().bounds.size.x;
			radius = radius * 0.5f;

			//Set this to be our new position for when we create the transition piece in game
			float newX = startX + proceduralSections [currentProceduralSection].GetComponent<LevelLoader> ().GetLength () * multiplier + radius;     
			newTransition = (GameObject)Instantiate (transitionPiece, new Vector3 (newX, startingPosition.y, 1), Quaternion.identity);
			newTransition.transform.parent = gameObject.transform;



			newX += radius + proceduralSections[nextProceduralSection].GetComponent<LevelLoader>().GetLength() * 5f;
			//float startingX = newTransition.transform.position.x + radius + (proceduralSections[nextProceduralSection].GetComponent<LevelLoader>().GetLength() * 0.5f) * multiplier;
			proceduralSections[nextProceduralSection].transform.position = new Vector3(newX, startingPosition.y, gameObject.transform.position.z);

			//We are fine up to this point! 
            //Attach a transitional pieces to the end of it
			newX += radius + proceduralSections[nextProceduralSection].GetComponent<LevelLoader>().GetLength() * multiplier;
            newTransition = (GameObject)Instantiate(transitionPiece, new Vector3(newX, startingPosition.y, 1), Quaternion.identity);
            newTransition.transform.parent = gameObject.transform;

            //Place the future section
			//newX = newTransition.transform.position.x + radius + proceduralSections[futureSection].GetComponent<LevelLoader>().GetLength() * 5f;
          	//proceduralSections[futureSection].transform.position = new Vector3(newX, startingPosition.y, gameObject.transform.position.z);
//
//            //Add the final transition piece  
//			newX = proceduralSections[futureSection].transform.position.x + (proceduralSections[futureSection].GetComponent<LevelLoader>().GetLength() * 0.5f) * multiplier + radius;
//            newTransition = (GameObject)Instantiate(transitionPiece, new Vector3(newX, startingPosition.y, 1), Quaternion.identity);
//            newTransition.transform.parent = gameObject.transform;        

		}
        
	}

   public void SwapSections()
    {
        float transitionalRadius = transitionPiece.GetComponent<SpriteRenderer>().bounds.size.x;
        transitionalRadius = transitionalRadius * 0.5f;

       //Need to get the position of the last placed transitional piece
        float nextX = newTransition.transform.position.x ;
		Debug.Log ("procedural position is" + nextX);

       //Get the next sections radius and add this onto the position of the transitional piece
        nextX += proceduralSections[currentProceduralSection].GetComponent<LevelLoader>().GetLength() * 5f + transitionalRadius;

       //Apply this to the section
        proceduralSections[currentProceduralSection].transform.position = new Vector3(nextX, startingPosition.y, startingPosition.z);

		//Add a transition piece
		nextX += proceduralSections [currentProceduralSection].GetComponent<LevelLoader> ().GetLength() * multiplier + transitionalRadius;
		newTransition = (GameObject)Instantiate(transitionPiece, new Vector3(nextX, startingPosition.y, 1), Quaternion.identity);
		newTransition.transform.parent = gameObject.transform;

		   
       currentProceduralSection = nextProceduralSection;


		if (proceduralSections.Count == 2) {
			if(currentProceduralSection == 0)
				nextProceduralSection = 1;

			else 
				nextProceduralSection =0;
		}
       else if (futureSection > 0)                                   //We still need to call spawning for the third placed section.
       {
           //First time calling this
           nextProceduralSection = futureSection;
           futureSection = 0;
       }
       else
       {
           nextProceduralSection = Random.Range(0, proceduralSections.Count);

           if (nextProceduralSection == currentProceduralSection)
               nextProceduralSection = Random.Range(0, proceduralSections.Count);

       }
        

		Debug.Log ("Next section is" + proceduralSections [nextProceduralSection].name);
      

   }

   public void StartSpawningInNextArea()
   {
        //We shall toggle the next area to be active and thus start the spawning
       proceduralSections[nextProceduralSection].GetComponent<LevelLoader>().ResetSection();
   }
}
