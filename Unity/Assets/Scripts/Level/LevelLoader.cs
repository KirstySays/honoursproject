﻿/*
 * Kirsty Fraser
 * Created 20.5.15
 * Last amended 21.6.15
 * Version 1.3
 * 
 * 
 * 1.3 Added : Updated variable names so they are more suitable for what they are being used for in the script.	
 * 
 * Purpose : Used to load in level prefabs when created. Will instanciate the new prefab before coming on screen and deleting the 
 * previous prefab thus not to kill memory. Pulls all of the levels within the Resources->Level folder with the tag 'procedural' and
 * will use these to fill up the List of all available prefabs, chosing one at random to pick for the next level to load.
 * 
 * Requires : 
 * A folder structure of Resources -> Level to access the information. All level prefabs to be tagged with either 'procedural' or 'static'. 
 */ 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelLoader : MonoBehaviour {

	[SerializeField]
	private GameObject startLevel;

	[SerializeField]
	private GameObject endLevel;				//The level to finish the area on.

    [SerializeField]
	private float prefabPadding;				//Allows better alignment of prefabs within the set area.

    [SerializeField]
    private AreaType e_AreaType;

	[SerializeField]
	private int areaLengths;							//the number of prefabs you will be able to fit into thie area.

 	private GameObject previousLevel;
	private GameObject currentLevel;
	private GameObject nextLevel;

	private BoxCollider2D currentLevelCollider;
	private BoxCollider2D nextLevelCollider;

	private float currentLevelRadius;
	private float nextLevelRadius;
	private int totalCorriderLevels;
    private int totalDungeonLevels;
	private int totalLavaLevels;

    List<GameObject> corriderLevelPrefabs;
    List<GameObject> dungeonLevelPrefabs;
	List<GameObject> lavaLevelPrefabs;

    public enum AreaType {Corrider, Dungeon, Lava};
 
   // private float length;             //Keeps a hold of the overall length of the section of procedural area.
    private int prefabAmount;          //Tracks the amount of prefabs we're allowed to have within this section.
    
	// Use this for initialization
	void Awake ()
	{
		//Checks on the editor side
		if (startLevel == null)
			Debug.LogError ("Please define the beginning level of the procedural section from the resources folder.");


		switch (e_AreaType)
		{
			case AreaType.Corrider :					//If the type is corrider then fill the corrider items list.
			{
				corriderLevelPrefabs = new List<GameObject> ();
				foreach(GameObject go in Resources.LoadAll("CorriderScene"))
				{
					if(go.tag == "procedural")              //FIX - do we need to consider this now with the new update?
					{
						corriderLevelPrefabs.Add(go);
					}
					
					
				}
			totalCorriderLevels = corriderLevelPrefabs.Count;
			break;
			}


			case AreaType.Dungeon : 
			{
				dungeonLevelPrefabs = new List<GameObject>();

				foreach (GameObject go in Resources.LoadAll("DungeonScene"))
				{
					dungeonLevelPrefabs.Add(go);
				}

				totalDungeonLevels = dungeonLevelPrefabs.Count;
				break;
			}
			case AreaType.Lava :
			{
				lavaLevelPrefabs = new List<GameObject>();

				foreach (GameObject go in Resources.LoadAll("LavaScene"))
				{
					lavaLevelPrefabs.Add(go);
				}

				totalLavaLevels = lavaLevelPrefabs.Count;
				break;
			}

		}

		//Scale the game object to be the designated length of the area.
		gameObject.transform.localScale = new Vector3 (areaLengths, 1, 1);

        //Move the section to starting position to compensate for scaling
        gameObject.transform.position = new Vector3(5 * areaLengths, gameObject.transform.position.y, 1);

		//Ensure's we cannot destroy this game object
	    DontDestroyOnLoad(this.gameObject);
	}

    void Update()
    {
        if(prefabAmount -1 > -1)
        {
            LoadNewLevel();
        }

    }

	public void LoadNewLevel()
	{

      if (prefabAmount > 0){

          if (prefabAmount == 1 && endLevel != null)                   //Catch this first incase we are only having 2 prefabs in and there is an end level
          {
              //Place the final piece
              nextLevel = Instantiate(endLevel);
              currentLevelCollider = currentLevel.GetComponent<BoxCollider2D>();
              currentLevelRadius = currentLevelCollider.size.x * 0.5f;


              nextLevel.transform.parent = this.gameObject.transform;				//Assigns the newly instantiated level to be a child of the procedural section
              nextLevelCollider = nextLevel.GetComponent<BoxCollider2D>();
              //nextLevel.transform.localScale = new Vector3(0.05f, 0.1f, 1);  
              nextLevelRadius = nextLevelCollider.size.x * 0.5f;


              AlignPrefab();

              Debug.Log ("Next level is now the end level");

          }

          //if (nextLevel == null) {
          //    //First time calling this
          //    currentLevel = nextLevel;

          //    Debug.Log ("We're calling this in here");
          //}

          else
          {
              Debug.Log("Hey I'm getting called");
              //Check which kind of content we need to generate
              switch (e_AreaType)
              {
                  case AreaType.Corrider:
                      {
                          //Carry this out for corrider.
                          ChooseNextPrefab(corriderLevelPrefabs, totalCorriderLevels);
                          break;
                      }

                  case AreaType.Dungeon:
                      {
                          //Carry this out for dungeon.
                          ChooseNextPrefab(dungeonLevelPrefabs, totalDungeonLevels);
                          break;
                      }

                  case AreaType.Lava:
                      {
                          //Carry this out for lava
                          ChooseNextPrefab(lavaLevelPrefabs, totalLavaLevels);
                          break;

                      }
              }
          }
          prefabAmount--;
			
		}

		Debug.Log ("Prefabs left" + prefabAmount);

        
	}

    void AlignPrefab()
    {
        Debug.Log("Aligning prefab based on" + currentLevel.name);
        //Align this to be at the end of currentlevel by taking the radius of the gameobject(parent)
        float startingX = currentLevelCollider.transform.position.x + currentLevelRadius + nextLevelRadius; // prefabPadding;
        float startingY = gameObject.transform.position.y;               //MAGIC NUMBERS

        Vector3 startingPosition = new Vector3(startingX, startingY, 0);

        nextLevel.transform.position = startingPosition;
    }



    //Takes in the list of loaded prefabs and the total number of levesl for that list
    //Allows us to choose which prefab will be used next depending on the content which we need to generate
    private void ChooseNextPrefab(List<GameObject> _contentList, int _numOfLevels)
    {
        int randIndex = Random.Range(0, _numOfLevels);
        GameObject newLoadedLevel = _contentList[randIndex];

        nextLevel = Instantiate(newLoadedLevel);
        nextLevel.transform.parent = this.gameObject.transform;				//Assigns the newly instantiated level to be a child of the procedural section
        nextLevelCollider = nextLevel.GetComponent<BoxCollider2D>();

        if (currentLevel != null)
        {
            currentLevelCollider = currentLevel.GetComponent<BoxCollider2D>();

            currentLevelRadius = currentLevelCollider.size.x * 0.5f;
            nextLevelRadius = nextLevelCollider.size.x * 0.5f;
           
         }


        currentLevel = nextLevel;

        AlignPrefab();


      
    }
	

    public void ResetSection()
    {
		BoxCollider2D procedCollider = gameObject.GetComponent<BoxCollider2D>();
        Vector3 startingPosition = this.gameObject.transform.parent.transform.position;

        currentLevelCollider = startLevel.GetComponent<BoxCollider2D>();
        currentLevelRadius = currentLevelCollider.size.x * 0.5f;

        currentLevel = startLevel;

		if (areaLengths > 2)
			startingPosition.x = gameObject.transform.position.x - 5*areaLengths + currentLevelRadius;                    //HACKY HAX

		else 
			startingPosition.x = gameObject.transform.position.x - currentLevelRadius;			//We're only fitting 2 in here

        startingPosition.y = gameObject.transform.position.y;
        startingPosition.z =-1;

        currentLevel = (GameObject)Instantiate(startLevel, startingPosition, Quaternion.identity);
        currentLevel.transform.parent = this.gameObject.transform;
        
        //currentLevel.transform.localScale = new Vector3(0.05f, 0.1f, 1);

		prefabAmount = areaLengths;

        //Deduct one from the prefab amount 
        prefabAmount--;

        //Load up the next level to follow.    
        LoadNewLevel();

       // StartCoroutine(GenerateContent());

    }

    public int GetLength()
    {
        return areaLengths;

    }

    IEnumerator GenerateContent()
    {
        for (int i = 0; prefabAmount > i; i-- )
        {
            LoadNewLevel();
        }

        yield return new WaitForSeconds(10f);
    }
}
