﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SectionManager : MonoBehaviour {

    public int numberOfActiveSections;
    public List<Section> sections = new List<Section>();
    private float resetTimer = 0.0f;
    [SerializeField] private SoundManager soundManager;


	// Use this for initialization
	void Start () {
        Initialise();
	}

    void OnEnable() {
        Messenger.AddListener("ResetAll", Reset);
    }

    void OnDisable() {
        Messenger.RemoveListener("ResetAll", Reset);
    }
	
	// Update is called once per frame
	void Update () {
        CheckSections();
	}

    void Initialise() {
        for (int i = 0; i < sections.Count; i++) {
            if (i > 0) {
                sections[i].previousSection = sections[i - 1];
            }
            sections[i].Snap();
        }
    }

    public int GetCurrentActiveSectionCount() {
        return sections.Count;
    }

    void CheckSections() {
        if (sections.Count > numberOfActiveSections) {
            //RemoveFirstSection();
        }
    }

    public void RemoveFirstSection() {  
        if (sections.Count > 0) {
            var section = sections[0];
            sections.RemoveAt(0);
            Destroy(section.gameObject);
        }
        
    }

    public void RemoveSection(Section section) {
        if (Time.time > resetTimer + 3.0f) {
            sections.Remove(section);
            Destroy(section.gameObject);
        }

    }

    public void AddSection(Section section) {
        Section previousSection = null;
        if (sections.Count > 0) {
            previousSection = sections.Last();
        }
        else {
            section.transform.position = transform.position;
        }

        section.previousSection = previousSection;
        sections.Add(section);
        foreach (var coin in section.GetComponentsInChildren<Coin>()) {
            coin.soundManager = soundManager; // #cancer
        }

        section.Snap();
    }

    public void Reset() {
        Debug.Log("Resetting Section Manager");
        foreach (var section in sections) {
            Destroy(section.gameObject);
        }
        sections.Clear();
        sections = new List<Section>();
        resetTimer = Time.time;
    }
}
