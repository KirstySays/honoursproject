﻿using UnityEngine;
using System.Collections;

public class Section : MonoBehaviour {
    public Section previousSection;
    public GameObject leftSnapPoint;
    public GameObject rightSnapPoint;

    public void Snap() {
        if (previousSection != null) {
            Vector3 mySnapOffset = leftSnapPoint.transform.localPosition;
            mySnapOffset.Scale(transform.localScale);

            Vector3 previousSnapOffset = previousSection.rightSnapPoint.transform.localPosition;
            previousSnapOffset.Scale(previousSection.transform.localScale);

            Vector3 previousSectionPosition = previousSection.transform.position;
            Vector3 mySectionPosition = previousSectionPosition + previousSnapOffset - mySnapOffset;

            transform.position = mySectionPosition;
        }
    }
}

