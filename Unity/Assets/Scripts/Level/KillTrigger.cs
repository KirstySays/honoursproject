﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class KillTrigger : MonoBehaviour {

    [SerializeField] private string causeOfDeath = "Designer Error";

    private ParticleSystem[] particleSplash;
   

    void Start() {
        GetComponent<Collider2D>().isTrigger = true;
        particleSplash = GetComponentsInChildren<ParticleSystem>();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            other.GetComponent<Player>().Kill("Dragon");         //Should really be done through the message system. 

            for (int i = 0; i < particleSplash.Length; i++) {
                particleSplash[i].Play();
            }          
        }
    }
}
