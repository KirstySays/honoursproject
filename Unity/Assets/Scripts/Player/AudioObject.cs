﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioClip))]
public class AudioObject : MonoBehaviour {
    public AudioClip clip;
    private AudioSource source;

    private bool started = false;

    // Use this for initialization
    public void Play() {
        source = gameObject.AddComponent<AudioSource>();
        source.clip = clip;
        source.loop = false;
        source.Play();
        started = true;
    }

    // Update is called once per frame
    void Update() {
        if (!source.isPlaying && started) {
            Destroy(this.gameObject);
        }
    }
}
