﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Track the distance the player travels during the game
/// </summary>
public class Distance : MonoBehaviour {

    private float startingX = 0.0f;
    [ReadOnly] [SerializeField] private float distanceTravelled = 0.0f;

	// Use this for initialization
	void Start () {
        startingX = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
        // Don't want to go backwards!!!
        var d = transform.position.x - startingX;
        if (d > distanceTravelled) {
            distanceTravelled = d;
        }
	}

    public float GetDistanceTravelled() {
        return distanceTravelled;
    }

    public void ResetDistance()
    {
        distanceTravelled = 0.0f;

    }
}
