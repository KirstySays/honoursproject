﻿using UnityEngine;
using System.Collections;
using Utility;
using System.Collections.Generic;

/// <summary>
/// Handle player state, motion and input
/// </summary>
[RequireComponent (typeof (GrapplingHook))]
public class Player : MonoBehaviour {
    [Header("Visuals")]
    [SerializeField] private Animator animator;
    
    [Header("Audio")]
    [SerializeField] private AudioMultipleSource audioSources;

    [Header("Collision")]
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float collisionDistance = 100f;
    [SerializeField] private List<GameObject> groundTouching = new List<GameObject>();
    [ReadOnly] public bool grounded = false;
    private Rigidbody2D rigidBody;   

    [Header("Movement")]
    [SerializeField] private float runSpeed = 20.0f;
    [SerializeField] private Vector2 knockbackForce = new Vector2(30000.0f, 30000.0f);
    [SerializeField] private InputModule inputModule;
    private GrapplingHook grapplingHook;

    [Header("Health")]
    [SerializeField] private float killDelay = 0.0f;                    //delay for the end game screen to come on.
    [ReadOnly] public bool isAlive;

    [Header("Tracking")]
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private Leaderboard leaderBoard;
    [SerializeField] private GameManager gameManager;

    [Header("DeltaDNA")]
    [SerializeField] private float timeSync;                    //How often do you wish to take the current position of the player (in sec)

   

    private Vector3 startPosition;
    private bool inMenu;
    private bool isTrackingPosition;
    private bool finishedSwing;

    //DELTA DNA TRACKING VARIABLES
    DeltaDNAEventBuilder eventBuilder;
   
    private string deathCause;                  //Used for reporting back to deltaDNA
    private float tapsOnScreen;                 //Used for reporting back to deltaDNA
    private string swingStartPos;
    private string swingEndPos;
    private int swingStartTime;
    private int swingTime;

	// Use this for initialization
	void Awake () {
        grapplingHook = GetComponent<GrapplingHook>();
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = new Vector2(0.0f, 0.0f);
        isAlive = true;
        inMenu = true;
        startPosition = transform.position;
        tapsOnScreen = 0;
        isTrackingPosition = true;
        finishedSwing = true;
        eventBuilder = new DeltaDNAEventBuilder();

        if(timeSync == 0)
        {
            timeSync = 10;         //Avoids the lagging motion of it being tracked all the time
        }
        
	}

    void OnEnable() {
        Messenger<string>.AddListener("PlayerDeath", Kill);
        Messenger.AddListener("ResetAll", Reset);
        Messenger.AddListener("ResetPlayer", Reset);

		Messenger.AddListener ("Pause", Pause);
		Messenger.AddListener ("Unpause", Unpause);
       
    }

    void OnDisable() {
        Messenger<string>.RemoveListener("PlayerDeath", Kill);
        Messenger.RemoveListener("ResetAll", Reset);
        Messenger.RemoveListener("ResetPlayer", Reset);

		Messenger.RemoveListener ("Pause", Pause);
		Messenger.RemoveListener ("Unpause", Unpause);
    }
    void Update() {
        // If click, store location for grapple
        if (inputModule.state == InputState.Pressed) {
            if (grapplingHook.CreateGrapple(inputModule.point, grounded)) {
                audioSources.PlayRandomSound();
                tapsOnScreen++;

                StartCoroutine(TrackPlayerInput());
            }
        }
        else if (inputModule.state == InputState.Released) {
            grapplingHook.ReleaseGrapple();
        }

        //if (!isAlive && !isLossConditionsProcessed) {
        //    ProcessLossConditions();
        //}
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (groundTouching.Count <= 0)
        {
            //TODO start recording time for player in the air
            swingStartTime = (int)Time.time;
            swingStartPos = " " + transform.position.x + ", " + transform.position.y;
            finishedSwing = false;
            Debug.LogError("FINISHED SWING IS FALSE");
            grounded = false;
        }
        else {
            grounded = true;
            Debug.LogError("GROUNDED AND " + finishedSwing);
            if (!finishedSwing)
            {
                //TODO stop the timer
                Debug.LogError("HEY WEVE STOPPED SWINGING!");
                swingTime = (int)Time.time - swingStartTime;
                Debug.Log("SWING TIME " + swingTime);
                //-end position
                swingEndPos = " " + transform.position.x + ", " + transform.position.y;
                //CAll to DeltaDNA
                
                eventBuilder.RecordPlayerSwingTime(swingStartPos, swingEndPos, swingTime);
                finishedSwing = true;
            }
        }

        UpdateAnimator();

        if (grounded && !inMenu) {
            if (rigidBody.velocity.x < runSpeed) {
                rigidBody.velocity = new Vector2(runSpeed, rigidBody.velocity.y);
            }
            else {
                rigidBody.velocity = Vector2.Lerp(rigidBody.velocity, new Vector2(runSpeed, rigidBody.velocity.y), Time.deltaTime);
            }
        }

        if(grounded && inMenu) {
            if(rigidBody.velocity.x < runSpeed) {
                rigidBody.velocity = new Vector2(runSpeed, rigidBody.velocity.y);
            }
            else
                rigidBody.velocity = Vector2.Lerp(rigidBody.velocity, new Vector2(runSpeed, rigidBody.velocity.y), Time.deltaTime);
        }

        //Take the position of the character
        if(!inMenu && isTrackingPosition)
        {
            StartCoroutine("TakeCharacterPosition", timeSync);
        }

	}


    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ground")) {
            var directionToCollided = collision.collider.transform.position - transform.position;
            directionToCollided.Normalize();
           // float distance = 100.0f;
            RaycastHit2D rayhit = Physics2D.Raycast(transform.position, directionToCollided, collisionDistance, layerMask.value);
            Vector2 normal = rayhit.normal;
            Vector3 myDir = rayhit.transform.TransformDirection(new Vector3(normal.x, normal.y, 0.0f));
            float error = 2.0f;
            if (myDir.ApproximatelyEqual(rayhit.transform.up, error)) {
                groundTouching.Add(collision.collider.gameObject);
            }
            else {
                Crash(normal);
            }
           
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("TRIGGER");
        if (other.tag == "Finish")
            Messenger<string>.Broadcast("GameStateChange", "GameOver");
    }

    //Stop the movement while we're in the menu while playing the game.
    public void Pause() {
        Debug.Log("Pausing the player");
        runSpeed = 0;
        inMenu = true;
        animator.enabled = false;       //Freezes the animator

    }

    public void Unpause() {
        Debug.Log("UnPausing the player");
        runSpeed = 10.0f;
        inMenu = false;
        animator.enabled = true;        //Unfreezes the animator
        animator.speed = 1;
    }


    void OnCollisionStay2D(Collision2D collision) {

    }

    void OnCollisionExit2D(Collision2D collision) {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ground")) {
            groundTouching.Remove(collision.collider.gameObject);
        }
    }

    /// <summary>
    /// Update the values of the attached animator
    /// </summary>
    void UpdateAnimator() {
        animator.SetBool("Grounded", grounded);
    }


    /// <summary>
    /// Cuts the grappling hook and applies a force away from the direction
    /// </summary>
    /// <param name="direction"></param>
    void Crash(Vector2 direction) {
        grapplingHook.ReleaseGrapple();

        Vector2 finalForce = direction;
        finalForce.x *= -1.0f;
        if (finalForce.y == 0.0f) {
            finalForce.y = 1.0f;
        }
        finalForce.Scale(knockbackForce);
        rigidBody.velocity = Vector2.zero;
        rigidBody.AddForce(finalForce);
    }

    public void Kill(string cause) {
        Debug.Log("Player was killed by " + cause);
        deathCause = cause;
        ProcessLossConditions();
        
        grapplingHook.ReleaseGrapple();
        DetachFromGround();
        StartCoroutine(KillWait(killDelay));
        gameManager.GameOver(cause);                 //// --------THis should be called by events... something is wrong
        isTrackingPosition = false;
        inMenu = true;                      //Sets this so that we do not track movement anymore after death
        
       
       
    }

    // Should look at splitting this into parts and putting into existing relevant scripts
    public void ProcessLossConditions() {       
        var scoreData = scoreManager.GenerateScore();
        leaderBoard.PostHighscore(scoreData.finalScore);

        //Report this back up to deltaDNA 
        string deathPosition = " " + transform.position.x + ", " + transform.position.y;
        int coinsCollected = scoreManager.GetCollectedCoins();
       
        eventBuilder.RecordDeath(deathPosition, deathCause, coinsCollected);

       
    }

    public float GetSpeed() {
        return rigidBody.velocity.x;
    }

    //Used when the player is attached to the side of a ground piece
    public void DetachFromGround() {
        grounded = false;
    }

    public void StartScoring() {
        scoreManager.Reset();
    }
    public void Reset() {
        grapplingHook.ReleaseGrapple();
        groundTouching.Clear();
        transform.position = startPosition;
        rigidBody.velocity = new Vector2(0.0f, 0.0f);
        isAlive = true;
        inMenu = false;
        isTrackingPosition = true;                  //Sets this back to true once we've started again
        tapsOnScreen = 0;
        foreach (var spriteRenderer in GetComponentsInChildren<SpriteRenderer>()) {
            spriteRenderer.enabled = true;
        }
    }

    IEnumerator KillWait(float waitFor) {
        yield return new WaitForSeconds(waitFor);
        isAlive = false;
        //animator.SetTrigger("DeathBy" + cause);

        //foreach (var spriteRenderer in GetComponentsInChildren<SpriteRenderer>())
        //{
        //    spriteRenderer.enabled = false;
        //}
    }

    IEnumerator TakeCharacterPosition(float timeSync)
    {
        isTrackingPosition = false;
      
        string currentPos;
        currentPos = " " + transform.position.x + ", " + transform.position.y;
        eventBuilder.RecordCurrentPosition(currentPos);

        yield return new WaitForSeconds(timeSync);
        isTrackingPosition = true;

    }

    // Purpose : To tell deltaDNA whenever the player has interacted with the game to move the character
    IEnumerator TrackPlayerInput()
    {
        Debug.Log("Tracking tap");
       
        string currentCharPos;
        currentCharPos = " " + transform.position.x + ", " + transform.position.y;
        eventBuilder.RecordPlayerInteraction(currentCharPos);
        yield return new WaitForFixedUpdate();
    }
}
