﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Utility;

/// <summary>
/// Grappling hook functionality
/// </summary>
public class GrapplingHook : MonoBehaviour {

    private List<Vector2> grapplePoints;
    private List<GameObject> grappleObjects;
    private List<GameObject> graphicSections;
    private Rigidbody2D rigidBody;

   
    private float desiredHookLength;

    [Header("Visuals")]
    [SerializeField] private Transform sectionBase;
    [SerializeField] private GameObject startSection;
    [SerializeField] private GameObject endSection;

    [Header("Collision")] 
    [SerializeField] private LayerMask layerMask;
    [SerializeField] [ReadOnly] private bool grappleActive = false;
    
    [Header("Speed modifiers")]
	[SerializeField] private float boostFromRopeMultiplier = 5.0f;       // Multiplier for boost player receives after letting go of rope
    [SerializeField] private float boostFromWalkingMultiplier = 1.0f;   // Multiplier for boost player receives when they grapple from running
	[SerializeField] private float grappleSpeed = 10.0f;
    [SerializeField] private float ropeWindSpeed = 2.0f;                 // Rate at which the rope adjusts it's length towards the desired length when over the max
   
    [Header("Rope length")]
    [SerializeField] [Range(0.0f, 1.0f)] private float ropeLengthAdjust = 0.9f;              // Amount of the overall length to adjust towards:  default is 0.9f -> 10% of the length when the grapple is created is removed 
    [SerializeField] private bool adjustRopeLengthInAir = false;
    [SerializeField] private float maxLength;
    [SerializeField] [ReadOnly] private float hookLength;
    
    [Header("Input")]
    [ReadOnly] private bool timeoutActive = false;
    [SerializeField] private float timeoutValue = 1.0f;

	/// <summary>
	/// Initialise various lists and gete references to components
	/// </summary>
	void Start () {
        grapplePoints = new List<Vector2>();
        grappleObjects = new List<GameObject>();
        graphicSections = new List<GameObject>();
        rigidBody = GetComponent<Rigidbody2D>(); 
		ReleaseGrapple ();
	}
	
	/// <summary>
	/// Run the grappling hook maths and render the current state
	/// </summary>
	void Update () {
        if (grappleActive) {
            DoGrappleMaths();
        }

        RenderSections();

        if (grapplePoints.Count < 1) {
            grappleActive = false;
        }        

        //Debug.Log("Grounded: " + Time.deltaTime + ", " + grounded);
        //DrawLine();
	}

    /// <summary>
    /// Handle physics checks for wrapping the grappling hook
    /// </summary>
    void FixedUpdate() {
        if (grappleActive) {
            // We should raycast to check if we still have line of sight on our grapple point
            var anchor = grapplePoints.Last();
            var pos2D = new Vector2(transform.position.x, transform.position.y);
            var direction = anchor - pos2D;

            var hit = Physics2D.Raycast(pos2D, direction, maxLength, layerMask.value);
            // If we can't see our previous grapple point then we are wrapping around an object and should create a point in the grapple for this

            if (hit.collider != null && !anchor.ApproximatelyEqual(hit.point, 0.1f)) {
                //Debug.Log(anchor + ", " + hit.point);
                Vector2 newAnchor = hit.point;

                // Add an offset to the achor so that the rope wraps around an object rather then embedding in it
                Vector2 offsetDir = hit.point - (Vector2)hit.collider.transform.position;
                offsetDir.Normalize();
                //newAnchor += offsetDir * wrapOffsetDistance;

                hookLength = Vector2.Distance(newAnchor, pos2D);
                desiredHookLength = hookLength;

                Debug.Log("Extending " + hit.collider.name + ", " + grappleObjects.Last().name);

                // Add the point to the list
                grapplePoints.Add(newAnchor);
                grappleObjects.Add(hit.collider.gameObject);
                AddSection();
            }

            // If we can see our previous grapple point and we are wrapping around an object then 
            // we should check the angle between the current direction vector and previous direction vector - if they are similar then we can unwrap one
            if (grapplePoints.Count >= 2) {
                // Check if we can see our previous grapple point and the angle between the two directions is the same
                var previousAnchor = grapplePoints[grapplePoints.Count - 2];
                var currentAnchor = grapplePoints[grapplePoints.Count - 1];
                var previousDirection = previousAnchor - pos2D;
                var currentDirection = currentAnchor - pos2D;

                previousDirection.Normalize();
                currentDirection.Normalize();

                var previousHit = Physics2D.Raycast(pos2D, previousDirection, maxLength, layerMask.value);

                if (previousAnchor.ApproximatelyEqual(previousHit.point, 0.1f) && previousDirection.ApproximatelyEqual(currentDirection, 0.1f)) {
                    Debug.Log("Hit point: " + previousHit.point + ", Anchor Point" + previousAnchor);
                    hookLength = Vector2.Distance(previousAnchor, pos2D);
                    desiredHookLength = hookLength;

                    // Remove a grapple point
                    grapplePoints.RemoveAt(grapplePoints.Count - 1);
                    grappleObjects.RemoveAt(grappleObjects.Count - 1);
                    RemoveSection();
                }
            }
        }
    }

    /// <summary>
    /// Perform the grappling hook motion mathematics
    /// </summary>
    void DoGrappleMaths() {
        if (grapplePoints.Count > 0) {
            // Get the current achor point - collection to be used for wrapping around objects
            var anchor = grapplePoints.Last();

            // Get the current distance to between the grapple and point
            float distance = Vector2.Distance(transform.position, anchor);

            hookLength = Mathf.Lerp(hookLength, desiredHookLength, Time.deltaTime * ropeWindSpeed);

            // Only do stuff if the rope would affect our movement
            if (distance > hookLength) {
                // Get position in 2d because Unity is a pain and still has three dimensions for 2d transforms
                Vector2 pos2d = new Vector2(transform.position.x, transform.position.y);

                // Get vector between position and point
                Vector2 delta = pos2d - anchor;
                delta.Normalize();

                pos2d -= delta * (distance - hookLength);

                // Adjust position, converting back to 3d because UNITY SUCKS
                transform.position = new Vector3(pos2d.x, pos2d.y, transform.position.z);

                // Multiply by dot product to get our velocity delta
                float x = Vector2.Dot(delta, rigidBody.velocity);
                delta *= x;
                rigidBody.velocity -= delta;
            }
        }
    }

    /// <summary>
    /// Initialise the grappling hook at the given point
    /// </summary>
    /// <param name="point">Point in world space to attach the hook</param>
    /// <param name="grounded">Whether or not the player is grounded</param>
    public bool CreateGrapple(Vector2 point, bool grounded) {
        bool success = false;
        if (timeoutActive) {
            return success;
        }

		// Raycast towards the given point to figure out actual attachment point
        var pos2D = new Vector2(transform.position.x, transform.position.y);
        var direction = point - pos2D;
        direction.Normalize();

        var hit = Physics2D.Raycast(pos2D, direction, maxLength, layerMask.value);
        if (hit.collider != null) {
           
            Vector2 anchor = hit.point;
            
            hookLength = Vector2.Distance(anchor, pos2D);
            desiredHookLength = hookLength * ropeLengthAdjust;

            if (!grounded && !adjustRopeLengthInAir) {
                desiredHookLength = hookLength;
            }

            Vector2 perp = direction;
            perp.x = direction.y;
            perp.y = -direction.x;
            perp *= grappleSpeed;
            rigidBody.velocity += (perp);

            // Add the point to the list
            grapplePoints.Add(anchor);
            grappleObjects.Add(hit.collider.gameObject);
            AddSection();

            grappleActive = true;

            if (grounded) {
                rigidBody.velocity += (direction * boostFromWalkingMultiplier);
            }
            success = true;
        }
        else {
            Debug.Log("No hit");
            success = false;

            
        }
        StartTimeout();

        return success;
    }

    /// <summary>
    /// Release the grappling hook, adding velocity in the current direction of motion based on the property "boostFromRopeMultiplier"
    /// </summary>
    public void ReleaseGrapple() {
        if (grapplePoints.Count > 0) {
            Vector2 direction = grapplePoints.Last() - (Vector2)transform.position;
            Vector2 perp = direction;
            perp.x = direction.y;
            perp.y = -direction.x;
            perp.Scale(rigidBody.velocity.normalized);
            rigidBody.velocity += (perp * boostFromRopeMultiplier);
        }
     
        grappleActive = false;
        Delete();

        
    }

    /// <summary>
    /// Instantiate and add a graphical section to the list
    /// </summary>
    void AddSection() {
        var section = Instantiate<Transform>(sectionBase).gameObject;
        graphicSections.Add(section);
    }
    /// <summary>
    /// Remove the most recent section from the list and immediately destroy it
    /// </summary>
    void RemoveSection()
    {
        var section = graphicSections.Last();
        graphicSections.Remove(section);
        DestroyImmediate(section);
    }

    /// <summary>
    /// Render the graphical sections and stretch and rotate appropriately
    /// </summary>
    void RenderSections() {
        if (grapplePoints.Count > 0) {
            Vector2 startPos = transform.position;
            Vector2 endPos = grapplePoints[0];

            RotateGameObject(startSection, startPos, endPos);

            var endPieceStartPos = startPos;
            if (grapplePoints.Count > 1) {
                endPieceStartPos = grapplePoints.Last();
            }
            RotateGameObject(endSection, endPieceStartPos, endPos);
            endSection.transform.position = endPos;

            int counter = grappleObjects.Count;
            foreach (GameObject go in graphicSections) {
                counter--;
                endPos = grapplePoints[counter];
                StretchGameObject(go, startPos, endPos);  
                startPos = endPos;

                if (counter <= 0) {
                    break;
                }
            } 
        }
        else {
            // TODO: Idle tongue animation
            Vector2 startPos = transform.position;
            Vector2 endPos = startPos + new Vector2(0.4f, 0.0f);
            RotateGameObject(startSection, startPos, endPos);
            RotateGameObject(endSection, startPos, endPos);
            endSection.transform.position = endPos;
        }
    }

    /// <summary>
    /// Rotate a given game object to align with the beginning and end points
    /// </summary>
    /// <param name="go">GameObject to rotate</param>
    /// <param name="begin">Start point</param>
    /// <param name="end">End Point</param>
    void RotateGameObject(GameObject go, Vector2 begin, Vector2 end)
    {
        var direction = end - begin;
        direction = Vector3.Normalize(direction);
        go.transform.right = direction;
    }
    /// <summary>
    /// Stretch a given game object to reach between the given points
    /// </summary>
    /// <param name="go">GameObject to rotate</param>
    /// <param name="begin">Start point</param>
    /// <param name="end">End Point</param>
    void StretchGameObject(GameObject go, Vector2 begin, Vector2 end) {
        go.transform.position = begin;
        RotateGameObject(go, begin, end);

		//Magic numbers maaaaaaaaaaaaaan
        var scale = new Vector3(0.175f, 0.175f, 1.0f);
        scale.x = Vector2.Distance(begin, end) * 0.72f;
        go.transform.localScale = scale;
    }

    /// <summary>
    /// Destroy all graphical sections and clear the lists of points and objects
    /// </summary>
	void Delete() {
		foreach (GameObject go in graphicSections) {
			Destroy(go);
		}

		graphicSections.Clear ();
		grapplePoints.Clear ();
        grappleObjects.Clear();
	}

    /// <summary>
    /// Start Timeout Coroutine
    /// </summary>
    void StartTimeout() {
        StartCoroutine(Timeout(timeoutValue));
    }

    /// <summary>
    /// Set the variable timeoutActive to false for the given duration
    /// </summary>
    /// <param name="duration">Time in seconds</param>
    /// <returns></returns>
    IEnumerator<float> Timeout(float duration) {
        timeoutActive = true;

        for (float timer = 0.0f; timer < duration; timer += Time.deltaTime) {
            yield return 0;
        }

        timeoutActive = false;
        yield return 0;
    }
}
