﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Collect any collectables it collides with
/// </summary>
public class Collector : MonoBehaviour {

    [SerializeField]
    private string tagToCollect = "collectable";
    [ReadOnly] [SerializeField] int count = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    /// <summary>
    /// Check for collisions with collectables
    /// If found, call the Collect function on the collectable and add to the counter
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == tagToCollect) {
            other.GetComponent<Collectable>().Collect();
            count++;
        }
    }

    public int GetCollectedCount() {
        return count;
    }

    public void ResetCoins()
    {
        count = 0;
    }
}
