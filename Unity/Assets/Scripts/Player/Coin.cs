﻿using UnityEngine;
using System.Collections;
using Utility;

/// <summary>
/// Extends the Collectable class with Coin specific animation
/// </summary>
public class Coin : Collectable {

    [SerializeField] private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
        Initialise();
	}
	
    void OnEnable()
    {
        Messenger.AddListener("ResetAll", Reload);
    }

    void OnDisable()
    {
        Messenger.RemoveListener("ResetAll", Reload);
    }

	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Animates the coin after collection
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator Animate() {
        // move and fade
        spriteRenderer.enabled = false;
        yield return 0;
    }

    protected void Reload()
    {
        gameObject.SetActive(true);
        gameObject.GetComponentInChildren<SpriteRenderer>().enabled = true;

    }
}
