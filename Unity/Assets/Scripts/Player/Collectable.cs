﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Base class for use with the Collector class
/// </summary>
public class Collectable : MonoBehaviour {

    [SerializeField] private AudioSource collectedSoundEffect;
    public  SoundManager soundManager;
    private ParticleSystem particleSystem;
    private DeltaDNAEventBuilder DDEventBuilder;

	// Use this for initialization
	void Start () {
        Initialise();
        //soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
	}
	
	// Update is called once per frame
    void Update() {
        
    }

    protected virtual void Initialise() {
        particleSystem = gameObject.GetComponent<ParticleSystem>();
        DDEventBuilder = new DeltaDNAEventBuilder();
       //   gameObject.SetActive(true);

    }


    /// <summary>
    /// "Collects" the item, playing a sound and animation, and destroying the item afterwards
    /// </summary>
    public virtual void Collect() {

        string currentPos = " " + transform.position.x + ", " + transform.position.y;
        DDEventBuilder.RecordCoinCollected(currentPos);
		if (particleSystem != null) {
			particleSystem.Play ();
		}

        if (collectedSoundEffect && soundManager.IsSFXOn()) {
            collectedSoundEffect.Play();
        }
       
        StartCoroutine(DestroyAfterAudioFinishes());             
        StartCoroutine(Animate());


    }

    /// <summary>
    /// Animate the collectable in some way
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator Animate() {

      
        yield return 0;
    }

    /// <summary>
    /// Destroy the object after the attached audio finishes playing
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator DestroyAfterAudioFinishes() {

        Debug.Log("Destro IEnumerator getting called");

        if (collectedSoundEffect != null)
        {
            while (collectedSoundEffect.isPlaying)
            {
                yield return 0;
            }
        }

        if(particleSystem != null)
        {
            while (particleSystem.IsAlive())
                yield return 0;
        }


        // Destroy(gameObject);
        //Just disable the coin instead
        //gameObject.SetActive(false);
               
       yield return 0;
    }

   
}
