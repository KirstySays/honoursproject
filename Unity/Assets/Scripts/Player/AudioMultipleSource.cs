﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioMultipleSource : MonoBehaviour {
    [SerializeField] private List<AudioClip> clips = new List<AudioClip>();
    [SerializeField] private SoundManager soundManager;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayRandomSound() {
        if (soundManager.IsSFXOn())
        {
            int r = Random.Range(0, clips.Count);
            PlaySound(r);
        }
    }

    public void PlaySound(int index) {
        if (index < clips.Count && index > 0) {
            var clip = clips[index];

            if (soundManager.IsSFXOn())
            {
                GameObject tempGO = new GameObject();
                AudioObject ao = tempGO.AddComponent<AudioObject>();
                ao.clip = clip;
                ao.Play();
                tempGO.transform.parent = this.transform;
            }
        }
    }
}
