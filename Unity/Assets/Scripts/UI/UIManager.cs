﻿/*
 * Author: Kirsty Fraser
 * Date Created: 21.7.15
 * Last amended : n.a
 * 
 * Version 1.0
 * 
 * Purpose : To control what elements should be on the screen at any given time and will activate those items to appear on screen or off.
 */  

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	[SerializeField]
	private GameObject bgImage;

	[SerializeField]
	private GameObject inGameMenu;

	[SerializeField]
	private GameObject creditsScreen;

	[SerializeField]
	private GameObject endGameScreen;

	[SerializeField]
	private GameObject gameLogo;

	[SerializeField]
	private GameObject settingsBtn;

	[SerializeField]
	private GameObject playBtn;

	[SerializeField]
	private GameObject creditsBtn;

	[SerializeField]
	private GameObject menuBtn;

	[SerializeField]
	private GameObject sfxBtn;

	[SerializeField]
	private GameObject musicBtn;

    [SerializeField]
    private GameObject exitBtn;

    [SerializeField]
    private GameObject distanceRuler;

    [SerializeField]
    private ScoreManager scoreManager;

    [SerializeField]
    private GameObject hudText;

    [SerializeField]
    private GameObject thanksScreen;

    [SerializeField]
    private bool isDareBuild;                   //Select whether we want the dare screen in or not. 


	//Used to identify which objects need to be moved. 
	public void ChangeScreen(string _newScreen)
	{
		List<GameObject> moveOffScreen = new List<GameObject>();
		List<GameObject> moveOnScreen = new List<GameObject>();;

		switch(_newScreen)
		{
		case "SETTINGS" : 				
			moveOffScreen.Add(settingsBtn);
			moveOffScreen.Add(playBtn);
			moveOffScreen.Add(creditsBtn);
            moveOffScreen.Add(hudText);
            MoveOffScreen(moveOffScreen);

			moveOnScreen.Add(menuBtn);
			moveOnScreen.Add (sfxBtn);
			moveOnScreen.Add (musicBtn);

            MoveOnScreen(moveOnScreen);

            gameLogo.SetActive(false);

			break;

		case "MENU":
            moveOffScreen.Add(menuBtn);
            moveOffScreen.Add(sfxBtn);
            moveOffScreen.Add(musicBtn);
            moveOffScreen.Add(creditsScreen);
            moveOffScreen.Add(hudText);
            if(isDareBuild)
            {
                moveOffScreen.Add(thanksScreen);
            }
            MoveOffScreen(moveOffScreen);

            moveOnScreen.Add(settingsBtn);
            moveOnScreen.Add(playBtn);
            moveOnScreen.Add(creditsBtn);

            settingsBtn.GetComponent<UITween>().Reset();
            gameLogo.GetComponent<UITween>().Reset();
            MoveOnScreen(moveOnScreen);

            gameLogo.SetActive(true);


			break;

		case "CREDITS" :

            moveOffScreen.Add(creditsBtn);
            moveOffScreen.Add(settingsBtn);
            moveOffScreen.Add(playBtn);
            moveOffScreen.Add(hudText);

            MoveOffScreen(moveOffScreen);

            moveOnScreen.Add(menuBtn);
            moveOnScreen.Add(creditsScreen);         

            MoveOnScreen(moveOnScreen);

            gameLogo.SetActive(false);
			break;

		case "INGAMESTTNGS" :
    
            moveOffScreen.Add(settingsBtn);
            moveOffScreen.Add(hudText);

            MoveOffScreen(moveOffScreen);
            
            moveOnScreen.Add(inGameMenu);
            moveOnScreen.Add(menuBtn);
            moveOnScreen.Add(musicBtn);
            moveOnScreen.Add(sfxBtn);

            MoveOnScreen(moveOnScreen);

          
			break;

		case "PLAY" :
            moveOffScreen.Add(playBtn);
            moveOffScreen.Add(creditsBtn);
            moveOffScreen.Add(bgImage);
            moveOffScreen.Add(endGameScreen);
            moveOffScreen.Add(inGameMenu);
            moveOffScreen.Add(sfxBtn);
            moveOffScreen.Add(musicBtn);
            moveOffScreen.Add(menuBtn);

            if (isDareBuild)
            {
                moveOffScreen.Add(exitBtn);

            }

            MoveOffScreen(moveOffScreen);

            moveOnScreen.Add(distanceRuler);
            moveOnScreen.Add(hudText);
            MoveOnScreen(moveOnScreen);

           
            settingsBtn.GetComponent<UITween>().MoveInGame();
            settingsBtn.GetComponent<UITween>().Scale();
                
            gameLogo.GetComponent<UITween>().Rotate();
            gameLogo.GetComponent<UITween>().Scale();


			break;

		case "ENDGAME" :
            moveOffScreen.Add(settingsBtn);
            moveOffScreen.Add(inGameMenu);
            moveOffScreen.Add(sfxBtn);
            moveOffScreen.Add(musicBtn);
            moveOffScreen.Add(menuBtn);
            moveOffScreen.Add(distanceRuler);
            moveOffScreen.Add(hudText);

            MoveOffScreen(moveOffScreen);

            moveOnScreen.Add(bgImage);
           // moveOnScreen.Add(playBtn);
            moveOnScreen.Add(endGameScreen);

            if(isDareBuild)
            {
                //Need to display the exit button to allow players to go back to the main menu
                moveOnScreen.Add(exitBtn);

                //Need to move the play button as well. 
                playBtn.GetComponent<UITween>().MoveInGame();
                
            }
            else if(!isDareBuild)
            {
                moveOnScreen.Add(playBtn);
            }


            MoveOnScreen(moveOnScreen);
			break;

        case "THANKS":
            thanksScreen.SetActive(true);

            moveOffScreen.Add(exitBtn);
            moveOffScreen.Add(endGameScreen);

            MoveOffScreen(moveOffScreen);

            break;


		}

		//currentScreen = _newScreen.ToString();
	}

	void MoveOffScreen(List<GameObject> _offScreenObjects)
	{
		//Go through the list of items and move them off screen
		for (int i = 0; i < _offScreenObjects.Count; i++) {
			_offScreenObjects[i].GetComponent<UITween>().MoveOffScreen();
		}

	}

	void MoveOnScreen(List<GameObject> onScreenObjects)
	{
		//Go through the list of items and move them on screen
		for (int i = 0; i < onScreenObjects.Count; i++) {
			onScreenObjects[i].GetComponent<UITween>().MoveOnScreen();
		}
	}
}
