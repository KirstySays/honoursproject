﻿using UnityEngine;
using System.Collections;

public class UIGameLogo : UIWidget {



	public override void OnEnable()
	{
		Debug.Log ("Calling UIGameLogo first");
		//Set the listeners here
		Messenger.AddListener ("Settings", Hide);
		//Messenger.AddListener ("Menu", Display);
		Messenger.AddListener ("Menu", Reset);
		Messenger.AddListener ("Credits", Hide);
        Messenger.AddListener("Play", Hide);

	}

	public override void OnDisable()
	{
		Debug.Log ("Calling UIGameLogo first");
		//Set the listeners here
		Messenger.RemoveListener ("Settings", Hide);
		//Messenger.AddListener ("Menu", Display);
		Messenger.RemoveListener ("Menu", Reset);
		Messenger.RemoveListener ("Credits", Hide);
        Messenger.RemoveListener("Play", Hide);
	}
	
	void Hide()
	{
		Debug.Log ("Calling hide");


		this.gameObject.transform.localScale = new Vector3 (0,0,0);
	}

	void Reset()
	{
		this.GetComponent<UITween> ().Reset ();
	}

}