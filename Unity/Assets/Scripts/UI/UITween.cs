﻿/*
 * 
 * Made by Kirsty, I've ran out of coffee so please excuse the lack of documentation on this.
 * 
 * 
 */
using UnityEngine;
using System.Collections;

//Adding this here so Akos stays a happy designer
public enum LoopType
{
    none,
    loop,
    pingPong
}

public enum EaseType
{
    easeInQuad,
    easeOutQuad,
    easeInOutQuad,
    easeInCubic,
    easeOutCubic,
    easeInOutCubic,
    easeInQuart,
    easeOutQuart,
    easeInOutQuart,
    easeInQuint,
    easeOutQuint,
    easeInOutQuint,
    easeInSine,
    easeOutSine,
    easeInOutSine,
    easeInExpo,
    easeOutExpo,
    easeInOutExpo,
    easeInCirc,
    easeOutCirc,
    easeInOutCirc,
    linear,
    spring,
    easeInBounce,
    easeOutBounce,
    easeInOutBounce,
    easeInBack,
    easeOutBack,
    easeInOutBack,
    easeInElastic,
    easeOutElastic,
    easeInOutElastic,
}


public class UITween : MonoBehaviour {


    // TODO  Amend these so that they are hidden unless checked for a cleaner inspector. 

    [Header("Read Onlys")]
    [SerializeField][ReadOnly]
    private bool isOnScreen;

    [Header("Transform Positions")]
    [SerializeField]
    private Transform offScreen;                               //How much to move the UI element by

    [SerializeField]
    private Transform onScreen;

    [SerializeField]
    private Transform inGame;

    [SerializeField]
    private Transform dareScreen;

    [Header ("Rotation")]
    [SerializeField]
    private Vector3 rotateUpdate;

    [Header ("Scaling")]
    [SerializeField]
    private Vector3 scaleUpdate;


    [Header("Looping & Easing")]
    [SerializeField]
    private EaseType eEaseType;                             //Choosing which kind of ease type to use for the UI element

    [SerializeField]
    private LoopType eLoopType;                              //Choosing which kind of loop type for using on the UI element

    [SerializeField]
    private bool isContinuous;

    [SerializeField]
    private bool includingChildren;

    [Header("Fading")]

    [SerializeField]
    private float fadeAlpha;

    [SerializeField]
    private float fadeAmount;

    [Header("Timing & Delays")]                      

    [SerializeField]
    private float timeToComplete;                           //Full time taken to complete the animation.
    [SerializeField]
    private float delay;                                    //Any time delay for the animation of the UI element

    [Header ("Audio")]
    [SerializeField]
    private AudioClip audioClip;

    [SerializeField]
    private SoundManager soundManager;

     [SerializeField]
    private AudioSource audioSource;


    //Variables not visable for the inspector
    private Vector3 startingScale;
    private bool isPunchingPos;



	// Use this for initialization
	void Start () {
        startingScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(isContinuous && isOnScreen)
        {
            FadeUpdate();

        }
	}


    public void MoveOnScreen()
    {
        iTween.MoveTo(this.gameObject, iTween.Hash("x", onScreen.position.x, "y", onScreen.position.y, "z", onScreen.position.z, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString(), "easeType", eEaseType.ToString()));
        isOnScreen = true;
       
    }

    public void MoveInGame()
    {
        iTween.MoveTo(this.gameObject, iTween.Hash("x", inGame.position.x, "y", inGame.position.y, "z", inGame.position.z, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString(), "easeType", eEaseType.ToString()));
        isOnScreen = true;
    }

    public void MoveOffScreen()
    {
        iTween.MoveTo(this.gameObject, iTween.Hash("x", offScreen.position.x, "y", offScreen.position.y, "z", offScreen.position.z, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString(), "easeType", eEaseType.ToString()));
        isOnScreen = false;

    }

    public void Rotate()
    {

        iTween.RotateAdd(this.gameObject, rotateUpdate, timeToComplete);
    }

    public void Scale()
    {
        iTween.ScaleTo(this.gameObject, scaleUpdate, timeToComplete);

    }

    public void PunchPositionCheck(Vector3 shakeAmount)
    {
        if(!isPunchingPos)
        {
            PunchPositionPlaying();
            PunchPosition(shakeAmount);
        }
    }

    void PunchPositionPlaying()
    {
        isPunchingPos = true;
    }

   public void PunchPositionStop()
    {

        isPunchingPos = false;
    }
    public void PunchPosition(Vector3 shakeAmount)
    {
        Debug.Log("Punching position");
        iTween.PunchPosition(this.gameObject, iTween.Hash("x", shakeAmount.x, "y", shakeAmount.y, "z", shakeAmount.z, "time", timeToComplete, "loopType", eLoopType.ToString(), "easeType", eEaseType.ToString(), "oncomplete", "PunchPositionStop"));

    }

    
    public void PlaySound()
    {
        if(soundManager.IsSFXOn())
            audioSource.PlayOneShot(audioClip);
    }

    public void FadeUpdate()
    {

        Debug.Log("FadeUpdate being called from " + gameObject.name);
        //iTween.FadeUpdate(this.gameObject, iTween.Hash("alpha", fadeAlpha, "includechildren", includingChildren, "time", timeToComplete));
        iTween.FadeTo(this.gameObject, iTween.Hash("alpha", fadeAlpha, "amount", fadeAmount, "includeChildren", includingChildren, "time", timeToComplete, "delay", delay, "easeType", eEaseType.ToString(), "loopType", eLoopType.ToString()));

    }

    public void ToggleUpdate()
    {
        Debug.Log("Calling toggle update from " + gameObject.name);
        isOnScreen = true;                  //Used for the children of any parent objects 

    }

    public void Reset()
    {
        gameObject.transform.localScale = startingScale;
        gameObject.transform.localRotation = Quaternion.identity;

    }
}
