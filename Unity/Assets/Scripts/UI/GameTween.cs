﻿using UnityEngine;
using System.Collections;

public enum TweenType
{
	RotationTween,
	PositionTween
}

public class GameTween : MonoBehaviour {

	[SerializeField]
	private TweenType tweenType;

	[SerializeField]
	private bool playOnStart;

    [SerializeField]
    private float delay;                                    //Any time delay for the animation of the UI element

	[SerializeField]
	private Vector3 rotation;

    [SerializeField]
    private EaseType eEaseType;                             //Choosing which kind of ease type to use for the UI element

    [SerializeField]
    private LoopType eLoopType;                             //Choosing which kind of loop type for using on the UI element

    [SerializeField]
    private float timeToComplete;                           //Full time taken to complete the animation.



	// Use this for initialization
	void Start () {
	
		if(tweenType == TweenType.RotationTween && playOnStart)
		{

			//iTween.RotateBy(gameObject, new Vector3(0f,0f,359f), 5f);
			iTween.RotateBy(gameObject, iTween.Hash ("amount", rotation, "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString(), "easeType", eEaseType.ToString()));

			//iTween.RotateTo(this.gameObject, iTween.Hash ("rotation", new Vector3(0,0,359), "time", timeToComplete, "delay", delay, "loopType", eLoopType.ToString(), "easeType", eEaseType.ToString()));
		}
	}
}
