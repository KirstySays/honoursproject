﻿using UnityEngine;
using System.Collections;

public class UISettingsBtn : UIWidget {

	public override void OnEnable()
	{
		Messenger.AddListener ("Menu", Reset);
		Messenger.AddListener ("Menu", MoveOnScreen);
		Messenger.AddListener ("Credits", MoveOffScreen);
		Messenger.AddListener ("InGameSettings", MoveOffScreen);
		Messenger.AddListener ("Play", MoveInGame);
		Messenger.AddListener ("Play", Scale);
		Messenger.AddListener ("EndGame", MoveOffScreen);
        Messenger.AddListener("Settings", MoveOffScreen);
		
	}
	
	public override void OnDisable()
	{
		Messenger.RemoveListener ("Menu", Reset);
		Messenger.RemoveListener ("Menu", MoveOnScreen);
		Messenger.RemoveListener ("Credits", MoveOffScreen);
		Messenger.RemoveListener ("InGameSettings", MoveOffScreen);
		Messenger.RemoveListener ("Play", MoveInGame);
		Messenger.RemoveListener ("Play", Scale);
		Messenger.RemoveListener ("EndGame", MoveOffScreen);
        Messenger.RemoveListener("Settings", MoveOffScreen);
	}

	void Reset()
	{
		this.GetComponent<UITween> ().Reset ();
	}

	void MoveOnScreen()
	{
		this.GetComponent<UITween> ().MoveOnScreen ();
	}

	void MoveOffScreen()
	{
		this.GetComponent<UITween> ().MoveOffScreen ();
	}

	void MoveInGame()
	{
		this.GetComponent<UITween> ().MoveInGame ();
	}

	void Scale()
	{
		this.GetComponent<UITween> ().Scale ();
	}
	

}
