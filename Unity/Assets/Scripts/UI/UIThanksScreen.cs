﻿using UnityEngine;
using System.Collections;

public class UIThanksScreen : UIWidget {

	public override void OnEnable()
	{
		Messenger.AddListener ("Menu", OffScreen);
		Messenger.AddListener ("Thanks", Display);
		
	}
	
	public override void OnDisable()
	{
		Messenger.RemoveListener ("Menu", OffScreen);
		Messenger.RemoveListener ("Thanks", Display);
	}
	
	void Hide()
	{
		this.gameObject.transform.localScale = new Vector3 (0,0,0);
	}
	
	void Reset()
	{
		this.GetComponent<UITween> ().Reset ();
	}

	void OffScreen()
	{
		this.GetComponent<UITween> ().MoveOffScreen ();

	}

	void Display()
	{
		this.gameObject.transform.localScale = new Vector3 (1, 1, 1);
	}
}
