﻿using UnityEngine;
using System.Collections;

public class UIPlayButton : UIWidget
{

	public override void OnEnable()
	{
		Messenger.AddListener ("Settings", MoveOffScreen);
		Messenger.AddListener ("Menu", MoveOnScreen);
		Messenger.AddListener ("Credits", MoveOffScreen);
		Messenger.AddListener ("Play", MoveOffScreen);
		Messenger.AddListener ("EndGame", MoveOnScreen);
        
	}
	
	public override void OnDisable()
	{
		Messenger.RemoveListener ("Settings", MoveOffScreen);
		Messenger.RemoveListener ("Menu", MoveOnScreen);
		Messenger.RemoveListener ("Credits", MoveOffScreen);
		Messenger.RemoveListener ("Play", MoveOffScreen);
		Messenger.RemoveListener ("EndGame", MoveOnScreen);
       
	}

	void MoveOffScreen()
	{
		this.GetComponent<UITween> ().MoveOffScreen ();
	}

	void MoveOnScreen()
	{
		this.GetComponent<UITween> ().MoveOnScreen ();
	}

	void Move()
	{
		
	}
}
