﻿using UnityEngine;
using System.Collections;

public class UIMusicButton : UIWidget {

    public override void OnEnable()
    {
        Messenger.AddListener("Settings", MoveOnScreen);
        Messenger.AddListener("InGameSettings", MoveOnScreen);
        Messenger.AddListener("Menu", MoveOffScreen);

    }

    public override void OnDisable()
    {
        Messenger.RemoveListener("Settings", MoveOnScreen);
        Messenger.RemoveListener("Menu", MoveOffScreen); 
        Messenger.RemoveListener("InGameSettings", MoveOnScreen);
    }

    void MoveOnScreen()
    {
        this.GetComponent<UITween>().MoveOnScreen();
    }

    void MoveOffScreen()
    {
        this.GetComponent<UITween>().MoveOffScreen();
    }

   
}
