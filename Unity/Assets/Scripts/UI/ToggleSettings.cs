﻿/*
 * Author : Kirsty Fraser
 * Date created : 29.6.15
 * Last amended : 
 * 
 * Version 1.0
 * 
 * Purpose : To change the sprites on the buttons for the settings screen to ensure they match with what the sound manager is
 * currently at.
 * 
 * 
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToggleSettings : MonoBehaviour {


    [SerializeField]
    private SoundManager soundManager;

    [SerializeField]
    private Sprite toggledOnSprite;

    
    [SerializeField]
    private Sprite toggledOffSprite;

    [SerializeField]
    private Button button;

    private bool isOn;

    void Start()
    {

        isOn = true;
    }


    //Function is called when the button is clicked
    public void UpdateSprite()
    {

        if(soundManager == null)
        {
            Debug.LogError("Put a sound manager into the editor you dummy!");
        }


        if(isOn)
        {
            isOn = false;
            button.image.sprite = toggledOffSprite;

            if (gameObject.name == "SFX")
            soundManager.TurnSFXOff();
            else if (gameObject.name == "Music")
            soundManager.TurnMusicOff();
        }
        else if (!isOn)
        {
            isOn = true;
            button.image.sprite = toggledOnSprite;

            if (gameObject.name == "SFX")
                soundManager.TurnSFXOn();
            else if (gameObject.name == "Music")
                soundManager.TurnMusicOn();

        }


        
    }
   
}
