﻿using UnityEngine;
using System.Collections;

public class UIMenuButton : UIWidget
{
    private bool isStillInMenu;
    public string gameState = "Null";

    public override void OnEnable()
    {
        Messenger.AddListener("Settings", SettingsGameState);
        Messenger.AddListener("InGameSettings", InGameSettingGameState);
        Messenger.AddListener("Settings", MoveOnScreen);
        Messenger.AddListener("InGameSettings", MoveOnScreen);
        Messenger.AddListener("Credits", MoveOnScreen);

        Messenger.AddListener("Menu", MoveOffScreen);
        Messenger.AddListener("Play", MoveOffScreen);
        Messenger.AddListener("EndGame", MoveOffScreen);

    }

    public override void OnDisable()
    {
        Messenger.RemoveListener("Settings", SettingsGameState);
        Messenger.RemoveListener("InGameSettings", InGameSettingGameState);
        Messenger.RemoveListener("Settings", MoveOnScreen);
        Messenger.RemoveListener("InGameSettings", MoveOnScreen);
        Messenger.RemoveListener("Credits", MoveOnScreen);

        Messenger.RemoveListener("Menu", MoveOffScreen);
        Messenger.RemoveListener("Play", MoveOffScreen);
        Messenger.RemoveListener("EndGame", MoveOffScreen);
    }

    void MoveOnScreen()
    {
        this.GetComponent<UITween>().MoveOnScreen();
    }

    void MoveOffScreen()
    {
        this.GetComponent<UITween>().MoveOffScreen();
    }

    void SettingsGameState()
    {
        isStillInMenu = true;
    }

    void InGameSettingGameState()
    {
        isStillInMenu = false;
    }

    public void OnClick()
    {
        Debug.Log("On Click being called");
        if (isStillInMenu)
            //Change the game manager state to the Main menu
            gameState = "MainMenu";
        else if (!isStillInMenu)
            //Change the game manager state to play 
            gameState = "Play";

        Messenger<string>.Broadcast("GameStateChange", gameState);

    }


}
