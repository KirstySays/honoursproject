﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//-- Context -- //
/*
To be used for registering & deregistering listeners for changing if they're on the screen or not. 
*/


public class UIWidget : MonoBehaviour {

    [SerializeField]
    public List<string> onScreenListenerEvents;                //Holds all the events that the widget needs to listen for for moving on screen

    [SerializeField]    
    public List<string> offScreenListenerEvents;               //Holds all the events that the widget needs to listen for moving off screen. 

   public virtual void OnEnable()
    {
		Debug.Log ("Calling UIWidget first");
        foreach(string listenerEvent in onScreenListenerEvents)
        {
            Messenger.AddListener(listenerEvent, GetComponent<UITween>().MoveOnScreen);
        }

        foreach (string listenerEvent in offScreenListenerEvents)
        {
            Messenger.AddListener(listenerEvent, GetComponent<UITween>().MoveOffScreen);
        }

    } 

    public virtual void OnDisable()
    {
        foreach (string listenerEvent in onScreenListenerEvents)
        {
            Messenger.RemoveListener(listenerEvent, GetComponent<UITween>().MoveOnScreen);
        }

        foreach (string listenerEvent in offScreenListenerEvents)
        {
            Messenger.RemoveListener(listenerEvent, GetComponent<UITween>().MoveOffScreen);
        }

    }


}
