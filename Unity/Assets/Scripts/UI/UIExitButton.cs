﻿using UnityEngine;
using System.Collections;

public class UIExitButton : UIWidget {

	public override void OnEnable()
	{
		Messenger.AddListener ("Play", MoveOffScreen);
		Messenger.AddListener ("EndGame", MoveOnScreen);
		Messenger.AddListener ("Thanks", MoveOffScreen);
		
	}
	
	public override void OnDisable()
	{
		Messenger.RemoveListener ("Play", MoveOffScreen);
		Messenger.RemoveListener ("EndGame", MoveOnScreen);
		Messenger.RemoveListener ("Thanks", MoveOffScreen);
	}

	void MoveOffScreen()
	{
		this.GetComponent<UITween> ().MoveOffScreen ();
	}

	void MoveOnScreen()
	{
		this.GetComponent<UITween> ().MoveOnScreen ();
	}
}
